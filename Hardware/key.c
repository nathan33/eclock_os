#include "stm32f10x.h"
#include "Key.h"
#include "Delay.h"

#define Key_RCCGroup	RCC_APB2Periph_GPIOB
#define Key_GPIOGroup	GPIOB
#define Key_Pins		GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15
#define Key1_Check		(GPIO_ReadInputDataBit(Key_GPIOGroup, GPIO_Pin_15) == 0)
#define Key2_Check		(GPIO_ReadInputDataBit(Key_GPIOGroup, GPIO_Pin_14) == 0)
#define Key3_Check		(GPIO_ReadInputDataBit(Key_GPIOGroup, GPIO_Pin_13) == 0)
#define Key4_Check		(GPIO_ReadInputDataBit(Key_GPIOGroup, GPIO_Pin_12) == 0)
#define Key5_Check		(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_8) == 0)

void Key_Init( void )
{
    RCC_APB2PeriphClockCmd( Key_RCCGroup, ENABLE );
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Pin = Key_Pins;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init( Key_GPIOGroup, &GPIO_InitStructure );
}

uint8_t Key_GetNumber( void )
{
    uint8_t KeyNum = 0;
    if ( Key1_Check )
    {
        Delay_ms( 20 );
        while ( Key1_Check );
        Delay_ms( 20 );
        KeyNum = 1;
    }
    if ( Key2_Check )
    {
        Delay_ms( 20 );
        while ( Key2_Check );
        Delay_ms( 20 );
        KeyNum = 2;
    }
    if ( Key3_Check )
    {
        Delay_ms( 20 );
        while ( Key3_Check );
        Delay_ms( 20 );
        KeyNum = 3;
    }
    if ( Key4_Check )
    {
        Delay_ms( 20 );
        while ( Key4_Check );
        Delay_ms( 20 );
        KeyNum = 4;
    }
    return KeyNum;
}
