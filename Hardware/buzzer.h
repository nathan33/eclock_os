#ifndef __BUZZER_H
#define __BUZZER_H

#include <stm32f10x.h>

void Buzzer_Init( void );

uint8_t Buzzer_GetStatus( void );

void Buzzer_On( void );

void Buzzer_Off( void );

void Buzzer_SetStatus( uint8_t newState );

uint8_t Buzzer_ToggleState( void );

void Buzzer_DelayBeep( uint16_t delayMS );

void Buzzer_CompareBeep( uint8_t i, uint8_t beepValue );

#endif
