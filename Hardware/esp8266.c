#include <esp8266.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_usart.h>

#define ESP8266_USART USART2
#define ESP8266_RST_RCC_Group RCC_APB2Periph_GPIOA
#define ESP8266_RST_GPIO_Port GPIOA
#define ESP8266_RST_Pin GPIO_Pin_6

char debugBuffer[256] = {0};
char buffer[128] = {0};
uint8_t ptr = 0;
uint8_t ptrTemp = 0xFF;


/**
 * Initialize esp8266 reset GPIO pin
 * @brief 初始化ESP8266的RST引脚
 */
void ESP8266_RSTPIN_Init( void )
{
    RCC_APB2PeriphClockCmd( ESP8266_RST_RCC_Group, ENABLE );
    GPIO_InitTypeDef GPIO_Initure;
    GPIO_Initure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Initure.GPIO_Pin = ESP8266_RST_Pin;
    GPIO_Initure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init( ESP8266_RST_GPIO_Port, &GPIO_Initure );
}


/**
 * Reset esp8266 by reset pin
 * @brief 重启一次ESP8266
 */
void ESP8266_Reset( void )
{
    GPIO_WriteBit( GPIOA, GPIO_Pin_6, Bit_RESET );
    Delay_ms( 250 );
    GPIO_WriteBit( GPIOA, GPIO_Pin_6, Bit_SET );
    Delay_ms( 500 );
}


/**
 * Clear receive buffer
 * @brief 清除ESP8266的接收数组以及重置数组指针
 */
void ESP8266_ClearBuffer( void )
{
    uint8_t i = 0;
    for ( i = 0; i < 127; i++ ) buffer[i] = 0;
    buffer[i] = 0;
    ptr = 0;
    ptrTemp = 0xFF;
}


/**
 * Send AT command
 * @brief 发送AT指令（带新行）
 * @param { char *ATCmd } AT command string
 */
void ESP8266_SendATCmd( char *ATCmd )
{
    ESP8266_ClearBuffer();
    UsartPrintf( ESP8266_USART, "%s", ATCmd );
    UsartPrintf( ESP8266_USART, "\r\n" );
}


/**
 * Get esp8266 receive buffer state
 * @brief 获取当前接收缓冲数组的状态（认为当ptr和ptrTemp两个指针相同且不为0时接收完成）
 * @return { ESP8266_ReceiveStateTypeDef } receive state
 */
ESP8266_RecStateTypeDef ESP8266_GetReceiveState( void )
{
    if ( ptr == 0 ) // 若ptr值为0，则表示esp8266处于等待接收的状态
    {
        return ESP8266_WAITING;
    }
    else if ( ptrTemp != ptr ) // 若ptrTemp不等于ptr，则表示esp8266处于正在接收的状态
    {
        ptrTemp = ptr;
        return ESP8266_RECEIVING;
    }
    else
    {
        buffer[ptr + 1] = 0;
        return ESP8266_RECEIVE_DONE;
    }
}


/**
 * Check the keyword of success is included in esp8266 received buffer
 * @brief 检测接收缓冲数组中是否包含有认为成功响应的子字符串
 * @param { char *kos } keyword of success
 * @return { uint8_t } check result
 */
ESP8266_ChkStateTypeDef ESP8266_CheckReceiveResult( char *kos )
{
    if ( ESP8266_GetReceiveState() == ESP8266_RECEIVE_DONE ) // 若接收完成则进行子串检测
    {
        if ( strstr( buffer, kos ) != NULL ) // 子串检测不为空则表示响应结果为成功
        {
            return ESP8266_CHK_SUCCESS;
        }
        else // 子串检测为空则表示响应结果为失败
        {
            return ESP8266_CHK_FAILED;
        }
    }
    else // 否则返回等待标志
    {
        return ESP8266_CHK_WATTING;
    }
}


/**
 * Run AT command ( run and check command running state )
 * @brief 运行一条AT指令并返回运行结果（通过循环调用以获取运行结果）
 * @param { char *ATCmd } AT command string
 * @param { char *kos } keyword of success
 * @param { uint16_t timeout } timeout (ms)
 * @return { ESP8266_RunStateEnumDef } run state
 */
ESP8266_RunStateTypeDef ESP8266_RunATCmd( char *ATCmd, char *kos, uint16_t timeout )
{
    ESP8266_RunStateTypeDef runState;
    uint8_t cmpRes = strcmp( ATCmd, "AT" );
    
    // Debug输出
    if ( cmpRes != 0 )
    {
        eprint( "<!-- ESP8266_RunATCmd -->" );
    }
    
    // 发送AT指令
    ESP8266_SendATCmd( ATCmd );
    
    // 检测循环
    while ( timeout )
    {
        // 若检测函数返回成功则赋值运行成功的状态并跳出循环
        if ( ESP8266_CheckReceiveResult( kos ) == ESP8266_CHK_SUCCESS )
        {
            runState = ESP8266_RUN_SUCCESS;
            break;
        }
        timeout--;
        Delay_ms( 1 );
    }
    
    // 超时检测
    if ( timeout == 0 )
    {
        runState = ESP8266_RUN_FAILED;
    }
    
    // Debug输出
    if ( cmpRes != 0 )
    {
        eprint( "%s", buffer );
    }
    
    Delay_ms( 12 );
    return runState;
}


/**
 * Send data string
 * @brief 发送数据
 * @param { unsigned char * } data string
 * @param { uint8_t } data string length
 */
uint8_t ESP8266_SendData( unsigned char *data, uint8_t len )
{
    uint8_t timeout = len * 2;
    char cipsendATCmd[16] = {0};
    
    eprint( "<!-- ESP8266_SendData -->" );
    
    sprintf( cipsendATCmd, "AT+CIPSEND=%d", len );
    ESP8266_SendATCmd( cipsendATCmd );
    eprint( cipsendATCmd );
    while ( ESP8266_CheckReceiveResult( ">" ) == ESP8266_CHK_SUCCESS && timeout )
    {
        timeout--;
        Delay_ms( 10 );
    }
    
    if ( timeout )
    {
        timeout = 30;
        Usart_SendString( ESP8266_USART, data, len );
        while ( ESP8266_CheckReceiveResult( "SEND OK" ) == ESP8266_CHK_SUCCESS && timeout )
        {
            timeout--;
            Delay_ms( 10 );
        }
        if ( timeout )
        {
            eprint( "Send ok" );
        }
        else
        {
            eprint( "Send timeout" );
        }
    }
    else
    {
        eprint( "AT timeout" );
    }
    
    Delay_ms( 12 );
    return !!( timeout );
}


/**
 * Get IPD (Get received data address)
 * @brief 获取IPD（获取接收数据的起始地址）
 * @param { uint16_t } timeout (ms)
 * @return { char * } IPD pointer in buffer
 */
char *ESP8266_GetIPD( uint16_t timeout )
{
    char *IPD = NULL;
    
    while ( timeout )
    {
        if ( ESP8266_GetReceiveState() == ESP8266_RECEIVE_DONE )
        {
            IPD = strstr( buffer, "IPD," );
            if ( IPD != NULL )
            {
                IPD = strchr( IPD, ':' );
                if ( IPD != NULL )
                {
                    IPD++;
                    break;
                }
            }
        }
        timeout--;
        Delay_ms( 1 );
    }
    
    if ( !timeout )
    {
        IPD = NULL;
    }
    
    return IPD;
}

/**
 * USART2 interrupt request handler
 * @brief USART2 中断函数
 */
void USART2_IRQHandler( void )
{
    if ( USART_GetITStatus( USART2, USART_IT_RXNE ) == SET )
    {
        if ( ptr >= 126 ) ptr = 0;
        buffer[ptr++] = USART2->DR;
        USART_ClearFlag( USART2, USART_FLAG_RXNE );
    }
}

/**
 * ESP8266 debug printer
 * @brief ESP8266 debug 输出函数
 * @param { char *strf } format
 * @params { ...args }
 */
void eprint( char *strf, ... )
{
    va_list arg;
    va_start( arg, strf );
    vsprintf( debugBuffer, strf, arg );
    va_end( arg );
    UsartPrintf( USART_DEBUG, "%s\r\n", debugBuffer );
}


/**
 * Get keyword address from receive buffer
 * @brief 匹配ESP8266接收数组中子串的位置
 * @param { char *keyword } keyword for finding
 * @return { char * } Receive buffer address
 */
char *ESP8266_FindStringFromBuffer( char *keyword )
{
    return strstr( buffer, keyword );
}

