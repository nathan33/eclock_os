#include "dht11.h"

#define DHT11_RCC           RCC_APB2Periph_GPIOA
#define DHT11_PORT          GPIOA
#define DHT11_PIN           GPIO_Pin_5
#define DHT11_PIN_RESET     GPIO_ResetBits(DHT11_PORT, DHT11_PIN)
#define DHT11_PIN_SET       GPIO_SetBits(DHT11_PORT, DHT11_PIN)
#define DHT11_PIN_READ      GPIO_ReadInputDataBit(DHT11_PORT, DHT11_PIN)

DHT11_DataTypeDef DHT11_Data = {0};

void DHT11_Init( void )
{
    RCC_APB2PeriphClockCmd( DHT11_RCC, ENABLE );
    DHT11_SetPinModeOut();
    DHT11_PIN_SET;
}

void DHT11_SetPinModeOut( void )
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = DHT11_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init( DHT11_PORT, &GPIO_InitStructure );
}

void DHT11_SetPinModeIn( void )
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = DHT11_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init( DHT11_PORT, &GPIO_InitStructure );
}

void DHT11_Reset( void )
{
    DHT11_PIN_RESET;
    Delay_ms( 20 );
    DHT11_PIN_SET;
    Delay_us( 30 );
}

void DHT11_Scan( void )
{
    uint8_t i, j;
    uint8_t buffer[5] = {0};
    uint16_t timeout = 0xFFFF;

    DHT11_SetPinModeOut();
    DHT11_Reset();
    DHT11_SetPinModeIn();

    while ( DHT11_PIN_READ == 1 && timeout )
        timeout--;
    while ( DHT11_PIN_READ == 0 && timeout )
        timeout--;

    for ( j = 0; j < 5; j++ )
    {
        for ( i = 0; i < 8; i++ )
        {
            while ( DHT11_PIN_READ && timeout )
                timeout--;
            while ( !DHT11_PIN_READ && timeout )
                timeout--;
            Delay_us( 40 );
            if ( DHT11_PIN_READ )
                buffer[j] |= ( uint8_t )( 0x01 << ( 7 - i ) );
            else
                buffer[j] &= ( uint8_t )~( 0x01 << ( 7 - i ) );
        }
    }

    DHT11_SetPinModeOut();
    DHT11_PIN_SET;

    if ( ( buffer[0] + buffer[1] + buffer[2] + buffer[3] ) == buffer[4] )
    {
        DHT11_Data.humidityInt      = buffer[0];
        DHT11_Data.humidityDec      = buffer[1];
        DHT11_Data.temperatureInt   = buffer[2];
        DHT11_Data.temperatureDec   = buffer[3];
        DHT11_Data.summary          = buffer[4];
    }
}
