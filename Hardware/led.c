#include "stm32f10x.h"   // Device header
#include "LED.h"

#define LED1_GPIO_PORT GPIOA
#define LED1_GPIO_PIN GPIO_Pin_1
#define LED1_CHECK (GPIO_ReadOutputDataBit(LED1_GPIO_PORT, LED1_GPIO_PIN) == Bit_RESET)

void LED_Init( void )
{
    // Enable APB clock
    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA, ENABLE );
    // Init LED GPIOs
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Pin = LED1_GPIO_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init( GPIOA, &GPIO_InitStructure );
    // Set LED pins
    GPIO_WriteBit( LED1_GPIO_PORT, LED1_GPIO_PIN, Bit_SET );
}


uint8_t LED1_GetStatus( void )
{
    return ( uint8_t )( LED1_CHECK );
}


void LED1_On( void )
{
    GPIO_WriteBit( LED1_GPIO_PORT, LED1_GPIO_PIN, Bit_RESET );
}


void LED1_Off( void )
{
    GPIO_WriteBit( LED1_GPIO_PORT, LED1_GPIO_PIN, Bit_SET );
}


void LED1_SetStatus( uint8_t newState )
{
    if ( newState )
    {
        LED1_On();
    }
    else
    {
        LED1_Off();
    }
}

uint8_t LED1_ToggleState( void )
{
    if ( LED1_CHECK )
    {
        LED1_Off();
        return 0;
    }
    else
    {
        LED1_On();
        return 1;
    }
}
