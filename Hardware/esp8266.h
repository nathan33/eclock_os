#ifndef __ESP8266_H
#define __ESP8266_H

#include <string.h>
#include <stdio.h>
#include <oled.h>
#include <delay.h>
#include <usart.h>
#include <stdarg.h>


/* ESP8266 ENUM ------------------ */

typedef enum
{
    ESP8266_WAITING = 0,
    ESP8266_RECEIVING,
    ESP8266_RECEIVE_DONE,
} ESP8266_RecStateTypeDef;

typedef enum
{
    ESP8266_CHK_WATTING = 0,
    ESP8266_CHK_SUCCESS,
    ESP8266_CHK_FAILED,
} ESP8266_ChkStateTypeDef;

typedef enum
{
    ESP8266_RUN_FAILED = 0,
    ESP8266_RUN_SUCCESS,
    ESP8266_RUN_TCPNOIP,
} ESP8266_RunStateTypeDef;

/* ------------------- ESP8266 ENUM */



/* ESP8266 FUNCTION PROTOTYPE ----- */

void ESP8266_RSTPIN_Init( void );

void ESP8266_Reset( void );

void ESP8266_ClearBuffer( void );

void ESP8266_SendATCmd( char *ATCmd );

ESP8266_RecStateTypeDef ESP8266_GetReceiveState( void );

ESP8266_ChkStateTypeDef ESP8266_CheckReceiveResult( char *kos );

ESP8266_RunStateTypeDef ESP8266_RunATCmd( char *ATCmd, char *kos, uint16_t timeout );

uint8_t ESP8266_SendData( unsigned char *data, uint8_t len );

char *ESP8266_GetIPD( uint16_t timeout );

void eprint( char *strf, ... );

char *ESP8266_FindStringFromBuffer( char *keyword );

/* ----- ESP8266 FUNCTION PROTOTYPE */


#endif
