#ifndef __LED_H
#define __LED_H

void LED_Init( void );

uint8_t LED1_GetStatus( void );

void LED1_On( void );

void LED1_Off( void );

void LED1_SetStatus( uint8_t newState );

uint8_t LED1_ToggleState( void );


#endif
