#ifndef __DHT11_H
#define __DHT11_H

#include <stm32f10x.h>
#include <delay.h>



typedef struct
{
    uint8_t humidityInt;
    uint8_t humidityDec;
    uint8_t temperatureInt;
    uint8_t temperatureDec;
    uint8_t summary;
} DHT11_DataTypeDef;



extern DHT11_DataTypeDef DHT11_Data;



void DHT11_Init( void );

void DHT11_SetPinModeOut( void );

void DHT11_SetPinModeIn( void );

void DHT11_Reset( void );

uint8_t DHT11_ReadByte( void );

void DHT11_Scan( void );


#endif
