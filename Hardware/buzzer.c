#include <stm32f10x.h>
#include <delay.h>

#define Buzzer_PinGroup GPIOA
#define Buzzer_RCCGroup RCC_APB2Periph_GPIOA
#define Buzzer_Pin GPIO_Pin_4


void Buzzer_Init( void )
{
    RCC_APB2PeriphClockCmd( Buzzer_RCCGroup, ENABLE );

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Pin = Buzzer_Pin;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init( Buzzer_PinGroup, &GPIO_InitStructure );
}


uint8_t Buzzer_GetStatus( void )
{
    return ( uint8_t )( GPIO_ReadOutputDataBit( Buzzer_PinGroup, Buzzer_Pin ) == Bit_SET );
}


void Buzzer_On( void )
{
    GPIO_WriteBit( Buzzer_PinGroup, Buzzer_Pin, Bit_SET );
}


void Buzzer_Off( void )
{
    GPIO_WriteBit( Buzzer_PinGroup, Buzzer_Pin, Bit_RESET );
}


void Buzzer_SetStatus( uint8_t newState )
{
    if ( newState )
    {
        Buzzer_On();
    }
    else
    {
        Buzzer_Off();
    }
}


uint8_t Buzzer_ToggleState( void )
{
    if ( GPIO_ReadOutputDataBit( Buzzer_PinGroup, Buzzer_Pin ) == Bit_SET )
    {
        Buzzer_Off();
        return 0;
    }
    else
    {
        Buzzer_On();
        return 1;
    }
}


void Buzzer_DelayBeep( uint16_t delayMS )
{
    Buzzer_On();
    Delay_ms( delayMS );
    Buzzer_Off();
    Delay_ms( delayMS );
}


void Buzzer_CompareBeep( uint8_t i, uint8_t beepValue )
{
    if ( i >= beepValue )
        Buzzer_On();
    else
        Buzzer_Off();
}
