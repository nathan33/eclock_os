#ifndef __ITF_PROGRESS_H
#define __ITF_PROGRESS_H

#include <itf.h>


typedef struct
{
    uint8_t isDone;
    uint8_t taskp;
    uint8_t taskRes;
} ITF_Progress_RtnTypeDef;


/* FUNCTION PROTOTYPE ----- */

ITF_Progress_RtnTypeDef ITF_Progress_Init( void );

ITF_Progress_RtnTypeDef ITF_Progress_Main( void );

ITF_Progress_RtnTypeDef ITF_Progress_SendDHTData( void );

ITF_Progress_RtnTypeDef ITF_Progress_SendData( void );

ITF_Progress_RtnTypeDef ITF_Progress_MQTTCheck( void );

ITF_Progress_RtnTypeDef ITF_Progress_MQTTConnect( void );

ITF_Progress_RtnTypeDef ITF_Progress_ESP8266( void );

/* ----- FUNCTION PROTOTYPE */





#endif
