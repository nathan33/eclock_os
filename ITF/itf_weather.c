#include <itf_weather.h>

const char *wcstrs[] = {"未知", "晴", "多云", "阴", "阵雨", "雨", "雷阵雨"};
const char *wstrs[] = {"Unknown", "Sunny", "Cloudy", "Overcast", "Shower", "Rainy", "leizhenyu"};
const char *wdcstrs[] = {"北", "东北", "东", "东南", "南", "西南", "西", "西北"};
const char *wdstrs[] = { "North", "NorthEast", "East", "SouthEast", "South", "SouthWest", "West", "NorthWest" };


ITF_Weather_DataTypeDef weather;
ITF_Weather_DataTypeDef *ITF_Weather_GetDataStruct( void ) { return &weather; }


/**
 * ITF_Weather_GetWeatherInfo
 * @brief 获取天气信息
 */
uint8_t ITF_Weather_GetWeatherInfo( void )
{
    uint8_t chk = 0;
    // 发布天气信息的请求消息
    chk = ITF_Task_SendWeatherReq( weather.adcode );
    // 若发布成功则接收返回的数据并进行处理
    if ( chk )
    {
        chk = ITF_Task_MQTTReceive( 128 );
    }
    // chk作为函数返回值
    return chk;
}


/**
 * ITF_Weather_ParseWeatherStr
 * @brief 截取返回的天气字符串中的数据
 * @param { char *str } 天气字符串
 */
void ITF_Weather_ParseWeatherStr( char *str )
{
    // 通过sscanf函数扫描字符串获取对应数据
    sscanf( str, "%6d,%1d,%2d,%2d,%1d,%1d", &weather.adcode, &weather.wcode,
            &weather.temperature, &weather.humidity, &weather.wdcode, &weather.wpower );
    // 获取天气的字符串
    weather.wstr = ( char * )wstrs[weather.wcode];
    weather.wcstr = ( char * )wcstrs[weather.wcode];
    // 获取风向的字符串
    weather.wdstr = ( char * )wdstrs[weather.wdcode];
    weather.wdcstr = ( char * )wdcstrs[weather.wdcode];
}
