#ifndef __ITF_UTILS_H
#define __ITF_UTILS_H

#include <itf.h>



/* FUNCTION PROTOTYPE --- */

void ITF_Utils_RenderFPS( uint8_t isDisplayTitle );

void ITF_Utils_RenderDate( Date_DateTypeDef *date, uint8_t oledX, uint8_t oledY, uint8_t size );

void ITF_Utils_RenderTime( Date_DateTypeDef *time, uint8_t oledX, uint8_t oledY, uint8_t size );

void ITF_Utils_RenderDHT11Data( DHT11_DataTypeDef *data, uint8_t oledX, uint8_t oledY );

void ITF_Utils_RenderMainInfo( int8_t hour );

void ITF_Utils_RenderCheckList( char *list[], uint8_t len, int8_t ptr, char *clist, uint8_t oledX,
                                uint8_t oledY );

void ITF_Utils_RenderSettingPtr( ITF_PtrTypeDef *ptr, uint8_t oledX, uint8_t oledY );

void ITF_Utils_RenderSettingId( char list[][5], int8_t ptr, uint8_t oledX, uint8_t oledY );

void ITF_Utils_MoveTime( Date_DateTypeDef *date, int8_t *ptr, int8_t step );

void ITF_Utils_MoveDate( Date_DateTypeDef *date, int8_t *ptr, int8_t step );

void ITF_Utils_PtrCheck( int8_t *ptr, uint8_t origin, uint8_t bound, uint8_t isReload );

uint8_t ITF_Utils_AlertITFCheck( void );

uint8_t ITF_Utils_FixDatetimeByAsctime( ITF_SNTPInfoTypeDef *SNTPInfo );

void ITF_Utils_RenderTitle( char *title );

uint8_t ITF_Utils_ParseJsonTypeKeys( char *type );

//uint8_t ITF_Utils_cjsonvHandler(cJSON *json_type, cJSON *json_value);

uint8_t ITF_Utils_MQTTPayloadHandler( ITF_MQTTRecvTypeDef *mqttRecv );

void ITF_Utils_RenderBigTime( Date_DateTypeDef *date, uint8_t oledX, uint8_t oledY );

void ITF_Utils_RenderBigDate( Date_DateTypeDef *date, uint8_t oledX, uint8_t oledY );

void ITF_Utils_RenderEnWeather( void );

void ITF_Utils_RenderZhWeather( void );

void ITF_Utils_MainTasksPreSec( void );

/* --- FUNCTION PROTOTYPE */



#endif
