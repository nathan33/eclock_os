#include <itf_stack.h>

ITF_ModeTypeDef modeStack[10];
uint8_t stackPtr = 0;

/**
 *
 */
void ITF_InitModeStack( void )
{
    uint8_t i = 0;
    for ( ; i < 10; i++ )
    {
        modeStack[i] = ITF_Main;
    }
    stackPtr = 0;
}

/**
 *
 */
ITF_ModeTypeDef ITF_GetMode( void )
{
    return modeStack[stackPtr];
}

/**
 *
 */
void ITF_Push( ITF_ModeTypeDef mode )
{
    stackPtr = ( stackPtr + 1 ) % 10;
    modeStack[stackPtr] = mode;
}

/**
 *
 */
ITF_ModeTypeDef ITF_Replace( ITF_ModeTypeDef mode )
{
    ITF_ModeTypeDef modeBkp;
    modeBkp = modeStack[stackPtr];
    modeStack[stackPtr] = mode;
    return modeBkp;
}

/**
 *
 */
ITF_ModeTypeDef ITF_GoBack( void )
{
    ITF_ModeTypeDef modeBkp;
    modeBkp = modeStack[stackPtr];
    modeStack[stackPtr] = ITF_Main;
    if ( stackPtr > 0 )
    {
        stackPtr -= 1;
    }
    return modeBkp;
}
