#include <itf_utils.h>

/**
 * Render FPS
 * @brief 渲染FPS数值
 * @param { uint8_t isDisplayTitle } title diaplay flag
 */
void ITF_Utils_RenderFPS( uint8_t isDisplayTitle )
{
    if ( isDisplayTitle )
    {
        // OLED_ShowImage(94, 0, 14, 8, fpsImg);
        OLED_ShowString( 90, 0, "FPS", OLED_6X8 );
    }
    OLED_ShowNum( 110, 0, FPS_Check(), 3, OLED_6X8 );
}


/**
 * Render date (year, month, mday)
 * @brief 渲染日期信息（年月日）
 * @param { Date_DateTypeDef *date } date
 * @param { uint8_t oledX } x value of oled
 * @param { uint8_t oledY } y value of oled
 * @param { uint8_t size } size of oled font (6/8)
 */
void ITF_Utils_RenderDate( Date_DateTypeDef *date, uint8_t oledX, uint8_t oledY,
                           uint8_t size )
{
    if ( size != 8 && size != 6 ) return;
    OLED_Printf( oledX, oledY, size, "20%02d/%02d/%02d", date->year, date->month,
                 date->day );
}


/**
 * Render time (hour, minute, second)
 * @brief 渲染时间信息（时分秒）
 * @param { Date_DateTypeDef *date } time
 * @param { uint8_t oledX } x value of oled
 * @param { uint8_t oledY } y value of oled
 * @param { uint8_t size } size of oled font (6/8)
 */
void ITF_Utils_RenderTime( Date_DateTypeDef *time, uint8_t oledX, uint8_t oledY,
                           uint8_t size )
{
    if ( size != 8 && size != 6 ) return;
    OLED_Printf( oledX, oledY, size, "%02d:%02d:%02d", time->hour, time->minute,
                 time->second );
}


/**
 * Render data of DHT11 (Humidity and temperature)
 * @brief 渲染DHT11温湿度
 * @param { DHT11_DataTypeDef *date } data of DHT11
 * @param { uint8_t oledX } x value of oled
 * @param { uint8_t oledY } y value of oled
 */
void ITF_Utils_RenderDHT11Data( DHT11_DataTypeDef *data, uint8_t oledX, uint8_t oledY )
{
    if ( data->temperatureInt == 0 && data->humidityInt == 0 )
    {
        OLED_ShowString( oledX, oledY, "Scanning", OLED_6X8 );
    }
    else
    {
        OLED_Printf( oledX, oledY, OLED_6X8, "%2d'C,%2d%%", data->temperatureInt,
                     data->humidityInt );
    }
}


/**
 * Render information on the ITF_Main (P/AM, flag of alarmclock and timer, weekday and month)
 * @brief 渲染主页信息
 * @param { int8_t hour } Current hour (For rendering P/AM)
 */
void ITF_Utils_RenderMainInfo( int8_t hour )
{
    OLED_ShowString( 0, 48, AlarmClock_GetIACActiveState() ? "(A)" : "   ", OLED_6X8 );
    OLED_ShowString( 0, 40, TimerClock_GetMTCActiveState() ? "(T)" : "   ", OLED_6X8 );
    OLED_ShowString( 100, 39, ( hour < 13 ) ? "AM" : "PM", OLED_6X8 );
    OLED_Printf( 0, 56, OLED_6X8, "%s %s", Date_GetWeekDayName(), Date_GetMonthName() );
}


/**
 * Render menu items
 * @brief 渲染菜单列表元素
 * @param { char *list[] } menu list
 * @param { uint8_t len } menu list length
 * @param { int8_t ptr } menu pointer
 * @param { char *clist } check list (sub)
 * @param { uint8_t oledX } x value of oled
 * @param { uint8_t oledY } y value of oled
 */
void ITF_Utils_RenderCheckList( char *list[], uint8_t len, int8_t ptr, char *clist,
                                uint8_t oledX, uint8_t oledY )
{
    uint8_t i;
    uint8_t clstc = ( clist != NULL );
    uint8_t sx = clstc ? oledX + 18 : oledX;
    
    OLED_ClearArea( 18, 10, 128 - 36, 64 - 8 );
    OLED_ShowString( 0, 32, "->", OLED_6X8 );
    
    for ( i = 1; i <= MIN( 2, ptr ); i++ )
    {
        OLED_ShowString( sx, oledY - i * 10, list[ptr - i], OLED_6X8 );
        if ( clstc ) OLED_Printf( oledX, oledY - i * 10, OLED_6X8, "[%c]", clist[ptr - i] );
    }
    OLED_ShowString( sx, oledY, list[ptr], OLED_6X8 );
    if ( clstc ) OLED_Printf( oledX, oledY, OLED_6X8, "[%c]", clist[ptr] );
    OLED_Printf( 128 - 4 * 6, oledY, OLED_6X8, "<%1d/%1d", ptr + 1, len );
    for ( i = 1; i < MIN( 3, len - ptr ); i++ )
    {
        OLED_ShowString( sx, oledY + i * 10, list[ptr + i], OLED_6X8 );
        if ( clstc ) OLED_Printf( oledX, oledY + i * 10, OLED_6X8, "[%c]", clist[ptr + i] );
    }
}


/**
 * Check value of pointer
 * @brief 检测菜单指针是否超出指定范围
 * @param { int8_t *ptr } pointer
 * @param { uint8_t origin } original value
 * @param { uint8_t bound } bound value
 * @param { uint8_t isReload } reload flag
 */
void ITF_Utils_PtrCheck( int8_t *ptr, uint8_t origin, uint8_t bound, uint8_t isReload )
{
    if ( *ptr > bound )
    {
        *ptr = isReload ? origin : bound;
    }
    else if ( *ptr < origin )
    {
        *ptr = isReload ? bound : origin;
    }
}


/**
 * Render setting pointer
 * @brief 渲染设置指针
 * @param { ITF_PtrTypeDef *ptr } pointer
 * @param { uint8_t oledX } x value of oled
 * @param { uint8_t oledY } y value of oled
 */
void ITF_Utils_RenderSettingPtr( ITF_PtrTypeDef *ptr, uint8_t oledX, uint8_t oledY )
{
    OLED_ShowChar( oledX + ( ptr->temp * 24 ) + 4, oledY - 16, ' ', OLED_8X16 );
    OLED_ShowChar( oledX + ( ptr->temp * 24 ) + 4, oledY + 16, ' ', OLED_8X16 );
    OLED_ShowImage( oledX + ( ptr->value * 24 ) + 4, oledY - 8, 8, 8, arrowUp );
    OLED_ShowImage( oledX + ( ptr->value * 24 ) + 4, oledY + 16, 8, 8, arrowDown );
}


/**
 * Render setting id
 * @brief 渲染设置字符串
 * @param { char list[][5] } id list
 * @param { int8_t ptr } pointer value
 * @param { uint8_t oledX } x value of oled
 * @param { uint8_t oledY } y value of oled
 */
void ITF_Utils_RenderSettingId( char list[][5], int8_t ptr, uint8_t oledX,
                                uint8_t oledY )
{
    OLED_ShowString( oledX, oledY, list[ptr], OLED_6X8 );
}


/**
 * Move time value
 * @brief 根据指针指向调用时间移动函数
 * @param { Date_DateTypeDef *date } date
 * @param { int8_t *ptr } pointer
 * @param { int8_t step } move step
 */
void ITF_Utils_MoveTime( Date_DateTypeDef *date, int8_t *ptr, int8_t step )
{
    if ( *ptr == 2 )
    {
        Date_MoveSecond( date, step, 0 );
    }
    else if ( *ptr == 1 )
    {
        Date_MoveMinute( date, step, 0 );
    }
    else
    {
        Date_MoveHour( date, step, 0 );
    }
}


/**
 * Move date value
 * @brief 根据指针指向调用日期移动函数
 * @param { Date_DateTypeDef *date } date
 * @param { int8_t *ptr } pointer
 * @param { int8_t step } move step
 */
void ITF_Utils_MoveDate( Date_DateTypeDef *date, int8_t *ptr, int8_t step )
{
    if ( *ptr == 2 )
    {
        Date_MoveDay( date, step, 0 );
    }
    else if ( *ptr == 1 )
    {
        Date_MoveMonth( date, step, 0 );
    }
    else
    {
        Date_MoveYear( date, step, 0 );
    }
}


/**
 * Alert ITF check
 * @brief 弹出层检测函数
 * @return { uint8_t } check result flag
 */
uint8_t ITF_Utils_AlertITFCheck( void )
{
    uint8_t c = 0;
    
    if ( TimerClock_GetMTCOverflowState() )
    {
        ITF_Push( ITF_TimerReach );
        c++;
    }
    
    if ( AlarmClock_GetIACReachState() )
    {
        ITF_Push( ITF_AlarmReach );
        c++;
    }
    
    return c;
}


/**
 * Render ITF title
 * @brief 渲染页面标题
 * @param { char *title } title
 */
void ITF_Utils_RenderTitle( char *title )
{
    uint8_t tlen = strlen( title );
    OLED_ShowString( 0, 0, title, OLED_6X8 );
    OLED_ReverseArea( 0, 0, tlen * 6 + 1, 8 );
    OLED_DrawLine( 0, 8, 128, 8 );
}


/**
 * @brief 将CJSON中的键转换为任务码
 */
// uint8_t ITF_Utils_ParseJsonTypeKeys( char *type )
// {
//     uint8_t i = 0;

//     for ( i = 0; i < 6; i++ )
//     {
//         if ( strcmp( type, typeKeys[i] ) == 0 ) break;
//     }

//     return i;
// }


/**
 * CJSON value handler
 * @brief 根据CJSON解析的类型执行对应的任务
 * @param { use8266_cjsonTypeDef *cjsonv } CJSON value
 * @return { uint8_t } Handle result flag
 */
// uint8_t ITF_Utils_cjsonvHandler( cJSON *json_type, cJSON *json_value )
// {
//     uint8_t handleRes = 0;

//     taskp = ITF_Tasks_ParseJsonTypeKeys( json_type->valuestring );

//     switch ( taskp )
//     {
//         case 0: Buzzer_SetStatus( json_value->valueint ); break;
//         case 1: LED1_SetStatus( json_value->valueint ); break;
//         case 2: AlarmClock_SetIACActiveState( json_value->valueint ); break;
//         case 3: AlarmClock_SetIACTimeByStr( json_value->valueint ); break;
//         case 4: AlarmClock_ConfirmIAC(); break;
//         case 5: Date_SetDateByAsctime( use8266_getSNTPTime() ); break;
//         default: taskp = 0xFF; break;
//     }

//     ESP8266_ClearBuffer();
//     return handleRes;
// }


/**
 * CJSON value handler
 * @brief 根据CJSON解析的类型执行对应的任务
 * @param { use8266_cjsonTypeDef *cjsonv } CJSON value
 * @return { uint8_t } Handle result flag
 */
// uint8_t ITF_Utils_MQTTPayloadHandler( ITF_MQTTRecvTypeDef *mqttRecv )
// {
//     uint8_t i = 0;
//     char *t = NULL;
//     char *p = mqttRecv->payload;

//     // mqttRecv->payload += mqttRecv->plen;
//     // *mqttRecv->payload = 0;
//     // for ( i = 0; i < mqttRecv->plen; i++ )
//     // {
//     //     sendBuffer[i] = *p++;
//     // }
//     // sendBuffer[i] = 0;
//     // sendBuffer[i + 1] = 0;
//     // p = sendBuffer;

//     t = strchr( mqttRecv->payload, '@' );
//     if ( t == NULL ) return 0;
//     *t = 0;
//     i = ITF_Utils_ParseJsonTypeKeys( ++t );

//     switch ( i )
//     {
//         case 0: Buzzer_SetStatus( atoi2( *p ) ); break;
//         case 1: LED1_SetStatus( atoi2( *p ) ); break;
//         case 2: AlarmClock_SetIACActiveState( atoi2( *p ) ); break;
//         case 3: AlarmClock_SetIACTimeByStr( p ); break;
//         case 4: AlarmClock_ConfirmIAC(); break;
//         case 5: Date_SetDateByAsctime( use8266_getSNTPTime() ); break;
//         default: i = 0xFF; break;
//     }

//     if ( ITF_GetMode() == ITF_MQTTReceive ) // 仅在MQTT消息接受页面输出DEBUG信息
//     {
//         OLED_ClearArea( 42, 10, 86, 20 );
//         OLED_ShowString( 42, 10, t, OLED_6X8 );
//         OLED_ShowString( 42, 20, p, OLED_6X8 );
//         OLED_ShowNum( 24, 10, i, 1, OLED_6X8 );
//     }

//     MQTT_FreeBuffer( mqttRecv->payload );
//     return i;
// }


/**
 * ITF_Utils_RenderBigTime
 * @brief 大数字的时间显示函数
 * @param { Date_DateTypeDef *date }
 * @param { uint8_t oledX }
 * @param { uint8_t oledY }
 */
void ITF_Utils_RenderBigTime( Date_DateTypeDef *date, uint8_t oledX, uint8_t oledY )
{
    char timeStr[9];
    uint8_t i = 0, j = 0;
    
    memset( timeStr, 0, sizeof timeStr );
    sprintf( timeStr, "%02d:%02d:%02d", date->hour, date->minute, date->second );
    
    for ( i = 0; i < sizeof timeStr - 1; i++ )
    {
        j = MIN( 12, ( timeStr[i] - '/' ) );
        OLED_ShowImage( oledX + i * 12 , oledY, 12, 24, OLED_F16x24[j] );
    }
}


/**
 * ITF_Utils_RenderBigDate
 * @brief 大数字的日期显示函数
 * @param { Date_DateTypeDef *date }
 * @param { uint8_t oledX }
 * @param { uint8_t oledY }
 */
void ITF_Utils_RenderBigDate( Date_DateTypeDef *date, uint8_t oledX, uint8_t oledY )
{
    char dateStr[11];
    uint8_t i = 0, j = 0;
    
    memset( dateStr, 0, sizeof dateStr );
    sprintf( dateStr, "20%02d/%02d/%02d", date->year, date->month, date->day );
    
    for ( i = 0; i < sizeof dateStr - 1; i++ )
    {
        j = MIN( 12, ( dateStr[i] - '/' ) );
        OLED_ShowImage( oledX + i * 12 , oledY, 12, 20, OLED_F16x24[j] );
    }
}


/**
 * ITF_Utils_RenderEnWeather
 * @brief 英文天气信息显示函数
 */
void ITF_Utils_RenderEnWeather( void )
{
    ITF_Weather_DataTypeDef *w = ITF_Weather_GetDataStruct();
    // 地区名称、天气
    OLED_Printf( 0, 10, OLED_6X8, "City   : XiangZhou" );
    OLED_Printf( 0, 19, OLED_6X8, "Weather: %s", w->wstr );
    // 温湿度
    OLED_Printf( 0, 28, OLED_6X8, "Temp   : %d", w->temperature );
    OLED_Printf( 0, 37, OLED_6X8, "Humid  : %d", w->humidity );
    // 风向、风力
    OLED_Printf( 0, 47, OLED_6X8, "WindDrt: %s", w->wdstr );
    OLED_Printf( 0, 56, OLED_6X8, "WindPwr: %d", w->wpower );
}


/**
 * ITF_Utils_RenderZhWeather
 * @brief 中文天气信息显示函数
 */
void ITF_Utils_RenderZhWeather( void )
{
    ITF_Weather_DataTypeDef *w = ITF_Weather_GetDataStruct();
    // 地区名称
    OLED_Show12Chinese( 0, 10, "地点：" );
    OLED_Show12Chinese( 36, 10, "香洲区" );
    // 天气
    OLED_Show12Chinese( 0, 24, "天气：" );
    OLED_Show12Chinese( 36, 24, w->wcstr );
    // 温度
    OLED_Show12Chinese( 0, 38, "温度：" );
    OLED_Printf( 34, 38 + 3, OLED_6X8, "%2d'C", w->temperature );
    // 湿度
    OLED_Show12Chinese( 64, 38, "湿度：" );
    OLED_Printf( 98, 38 + 3, OLED_6X8, "%2d %%", w->humidity );
    // 风向、风力
    OLED_Show12Chinese( 0, 52, "风向：" );
    OLED_Show12Chinese( 36, 52, w->wdcstr );
    OLED_Show12Chinese( 64, 52, "风力：" );
    OLED_ShowNum( 98, 52 + 3, w->wpower, 1, OLED_6X8 );
    OLED_Show12Chinese( 128 - 20, 52, "级" );
}


/**
 * ITF_Utils_MainTasksPreSec
 * @brief MainRender中秒级任务的处理函数
 */
void ITF_Utils_MainTasksPreSec( void )
{
    if ( DSTemp.second != DS->second )
    {
        DSTemp.second = DS->second;
        ITF_Utils_AlertITFCheck();
        if ( ITF_Task_MQTTReceive( 32 ) )
        {
            if ( IFlags.MQTTReceive_TaskRtn )
            {
                ITF_Push( ITF_MQTTReceive );
            }
        }
        else
        {
            if ( !p3 ) DHT11_Scan();
            if ( !p5 && !IFlags.Main_JTMS ) ITF_Progress_MQTTConnect();
            if ( !p10 ) ITF_Task_SendAllData();
            if ( !p60 ) ITF_Progress_MQTTCheck();
        }
    }
}
