#include <itf.h>

uint8_t ITF_IWDGRSTCNT = 0;
char sendBuffer[128] = {0};
const char *typeKeys[] = {"bz", "led", "iac-s", "iac-t", "iac-c", "sntp", "weather"};
char okString[][7] = {"NOT OK", "  OK  "};

Date_DateTypeDef DSTemp;
Date_DateTypeDef *DS;

ITF_FlagsTypeDef IFlags;
ITF_APInfoTypeDef APInfo;
ITF_MQTTInfoTypeDef MQTTInfo;
ITF_SNTPInfoTypeDef SNTPInfo;

ITF_NETInfoTypeDef NETInfo;


/**
 * ITF_Mount
 * @brief 系统入口
 */
void ITF_Mount( void )
{
    while ( 1 )
    {
        switch ( ITF_GetMode() )
        {
            // 0 - 9
            case ITF_Init:                  ITF_RenderInit(); break;
            case ITF_Main:                  ITF_RenderMain(); break;
            case ITF_Menu:                  ITF_RenderMenu(); break;
            case ITF_MQTTReceive:           ITF_RenderMQTTReceive(); break;
            case ITF_Main_DateTime:         ITF_RenderMain2(); break;
            // 10 - 19
            case ITF_Settings:              ITF_RenderSettings(); break;
            case ITF_TimeSetting:           ITF_RenderTimeSetting(); break;
            case ITF_DateSetting:           ITF_RenderDateSetting(); break;
            case ITF_AlarmSetting:          ITF_RenderAlarmSetting(); break;
            case ITF_AlarmTimeSetting:      ITF_RenderAlarmTimeSetting(); break;
            case ITF_AlarmRepeatDaySetting: ITF_RenderAlarmRepeatDaySetting(); break;
            case ITF_AlarmReach:            ITF_RenderAlarmReach(); break;
            case ITF_SNTPSetting:           ITF_RenderSNTPSetting(); break;
            case ITF_SNTPInfo:              ITF_RenderSNTPInfo(); break;
            case ITF_DebugSetting:          ITF_RenderDebugSetting(); break;
            // 20 - 29
            case ITF_Stopwatch:             ITF_RenderStopwatch(); break;
            // 30 - 39
            case ITF_Timer:                 ITF_RenderTimer(); break;
            case ITF_TimerTimeSetting:      ITF_RenderTimerTimeSetting(); break;
            case ITF_TimerReach:            ITF_RenderTimerReach(); break;
            // 40 - 49
            case ITF_ESP8266:               ITF_RenderESP8266(); break;
            case ITF_ESP8266_SelectAP:      ITF_RenderSelectAP(); break;
            // 50
            case ITF_Weather:               ITF_RenderWeather(); break;
            // 60
            case ITF_DailyReport:           ITF_RenderDailyReport(); break;
            // 70
            case ITF_SystemInfo:            ITF_RenderSystemInfo(); break;
            // else
            default:
                ITF_InitModeStack();
                ITF_Replace( ITF_Main );
                break;
        }
    }
}
