#ifndef __ITF_STACK_H
#define __ITF_STACK_H

#include <itf.h>


/* FUNCTION PROTOTYPE --- */

void ITF_InitModeStack( void );

ITF_ModeTypeDef ITF_GetMode( void );

void ITF_Push( ITF_ModeTypeDef mode );

ITF_ModeTypeDef ITF_Replace( ITF_ModeTypeDef mode );

ITF_ModeTypeDef ITF_GoBack( void );

/* --- FUNCTION PROTOTYPE */



#endif
