#ifndef __ITF_WEATHER_H
#define __ITF_WEATHER_H


#include <itf.h>
#include <inttypes.h>


typedef struct
{
    int adcode;
    int wcode;
    char *wstr;
    char *wcstr;
    int temperature;
    int humidity;
    int wdcode;
    char *wdstr;
    char *wdcstr;
    int wpower;
} ITF_Weather_DataTypeDef;


ITF_Weather_DataTypeDef *ITF_Weather_GetDataStruct( void );

uint8_t ITF_Weather_GetWeatherInfo( void );

void ITF_Weather_ParseWeatherStr( char *str );


#endif
