#include <itf_tasks.h>


/**
 * ITF_Task_InitDate
 * @brief 日期初始化
 */
uint8_t ITF_Task_InitDate( void )
{
    DSTemp.year = 24;
    DSTemp.month = 1;
    DSTemp.day = 1;
    DSTemp.hour = 0;
    DSTemp.minute = 0;
    DSTemp.second = 0;
    Date_Init( &DSTemp );
    return 1;
}


/**
 * ITF_Task_InitAlarmClock
 * @brief 闹钟初始化
 */
uint8_t ITF_Task_InitAlarmClock( void )
{
    AlarmClock_TimeTypeDef time;
    time.hour = 7;
    time.minute = 15;
    AlarmClock_SetIACTime( &time );
    AlarmClock_SetIACRepeatDay( 0xFE ); // 0xFE = 11111110 = 1234567-
    AlarmClock_SetIACActiveState( 1 );
    return 1;
}


/**
 * ITF_Task_InitNETInfo
 * @brief NETInfo结构体初始化
 */
uint8_t ITF_Task_InitNETInfo( void )
{
    NETInfo.APs[0].ssid = "Nathan33";
    NETInfo.APs[0].passwd = "19991112";
    NETInfo.APs[1].ssid = "LR_2.4G";
    NETInfo.APs[1].passwd = "LR_2.4G@PHICOMM";
    NETInfo.host = "8.222.190.33";
    NETInfo.subTopic = "/nathan/sub";
    NETInfo.pubTopic = "/nathan/pub";
    NETInfo.port = 1883;
    NETInfo.keepAlived = 0;
    NETInfo.ptr = 0;
    NETInfo.japRetry = 3;
    NETInfo.apChk = 0;
    NETInfo.tcpChk = 0;
    NETInfo.mqttChk = 0;
    NETInfo.mqttSubChk = 0;
    return 1;
}


/**
 * ITF_Task_InitSNTPInfo
 * @brief SNTPInfo结构体初始化
 */
uint8_t ITF_Task_InitSNTPInfo( void )
{
    SNTPInfo.sntpChk = 0;
    SNTPInfo.autoFetch = 1;
    SNTPInfo.enable = 1;
    SNTPInfo.timezone = 8;
    SNTPInfo.servers[0] = "cn.ntp.org.cn";
    SNTPInfo.servers[1] = "ntp2.baidu.com";
    SNTPInfo.servers[2] = "ntp.neu.edu.cn";
    return 1;
}


/**
 * ITF_Task_CheckAPStatus
 * @brief 检测AP状态
 */
uint8_t ITF_Task_CheckAPStatus( uint8_t isForce )
{
    // 获取（缓存）当前APs数组的ptr
    uint8_t i = NETInfo.ptr;
    // 当apChk为0时则不需要检测当前的AP状态，但isForce为1时除外
    if ( !NETInfo.apChk && !isForce ) return 0;
    // 检测所有AP信息，确定所连接AP的状态
    do
    {
        if ( use8266_isJoinedAP( &NETInfo.APs[i] ) )
        {
            NETInfo.apChk = 1;
            NETInfo.ptr = i;
            NETInfo.japRetry = 3;
            break;
        }
        else
        {
            NETInfo.apChk = 0;
            i = ( i + 1 ) % 2;
        }
    }
    while ( i != NETInfo.ptr );
    // 若检测到AP状态为连接失败，则需要将tcpChk、mqttChk以及mqttSubChk置0
    if ( !NETInfo.apChk )
    {
        NETInfo.tcpChk = 0;
        NETInfo.mqttChk = 0;
        NETInfo.mqttSubChk = 0;
    }
    // apChk作为函数返回值
    return NETInfo.apChk;
}


/**
 * ITF_Task_JoinAP
 * @brief 连接至AP
 * @param { uint8_t isForce } 强制连接标记
 */
uint8_t ITF_Task_JoinAP( uint8_t isForce )
{
    // 获取（缓存）当前APs数组的ptr
    uint8_t i = NETInfo.ptr;
    // 若japRetry为0则不进行JoinAP操作
    if ( !NETInfo.japRetry ) return 0;
    // 若apChk为1则不进行JoinAP操作，避免重复连接，但isForce为1时除外
    if ( NETInfo.apChk && !isForce ) return 0;
    // 尝试连接APs数组中的所有AP
    do
    {
        if ( use8266_joinAP( &NETInfo.APs[i] ) )
        {
            NETInfo.ptr = i;
            NETInfo.apChk = 1;
            break;
        }
        else
        {
            NETInfo.apChk = 0;
            i = ( i + 1 ) % 2;
        }
    }
    while ( i != NETInfo.ptr );
    // 检测apChk是否为0，为0则japRetry递减
    if ( !NETInfo.apChk )
    {
        NETInfo.japRetry -= 1;
        NETInfo.tcpChk = 0;
        NETInfo.mqttChk = 0;
        NETInfo.mqttSubChk = 0;
    }
    // apChk作为函数返回值
    return NETInfo.apChk;
}


/**
 * ITF_Tasks_CheckTCPStatus
 * @brief 检测TCP连接状态
 */
uint8_t ITF_Task_CheckTCPConnStatus( uint8_t isForce )
{
    // 若apChk为0则表示AP连接状态为失败，不进行TCP的连接检测
    if ( !NETInfo.apChk ) return 0;
    // 若tcpChk为0则不进行TCP的连接检测，但isForce为1时除外
    if ( !NETInfo.tcpChk && !isForce ) return 0;
    // 检测当前TCP连接状态
    if ( use8266_isConnected() )
    {
        NETInfo.tcpChk = 1;
    }
    else
    {
        NETInfo.tcpChk = 0;
        // 若TCP连接断开，则重置mqttChk以及mqttSubChk
        NETInfo.mqttChk = 0;
        NETInfo.mqttSubChk = 0;
    }
    // tcpChk作为函数返回值
    return NETInfo.tcpChk;
}


/**
 * ITF_Tasks_MakeTCPConn
 * @brief 创建TCP连接
 * @param { uint8_t isForce } 强制连接标记
 */
uint8_t ITF_Task_MakeTCPConn( uint8_t isForce )
{
    // 若apChk为0则不进行TCP连接操作
    if ( !NETInfo.apChk ) return 0;
    // 若tcpChk为1也不进行TCP连接操作，但isForce为1除外
    if ( NETInfo.tcpChk && !isForce ) return 0;
    // 创建TCP连接
    switch ( use8266_makeTCPConnection( NETInfo.host, NETInfo.port, NETInfo.keepAlived ) )
    {
        case 1: NETInfo.tcpChk = 1; break;
        case 2: NETInfo.apChk =
                0; // 若创建TCP连接时返回"no ip"则表示AP状态失效，置apChk为0
        case 0: NETInfo.tcpChk = 0; break;
    }
    // 重置mqttChk以及mqttSubChk
    NETInfo.mqttChk = 0;
    NETInfo.mqttSubChk = 0;
    // tcpChk作为函数返回值
    return NETInfo.tcpChk;
}


/**
 * ITF_Tasks_MakeMQTTConn
 * @brief 创建MQTT连接
 * @param { uint8_t isForce } 强制连接标记
 */
uint8_t ITF_Task_MakeMQTTConn( uint8_t isForce )
{
    // 准备subTopics（二维）
    const char *subTopics[1] = { NETInfo.subTopic };
    // 若TCP连接失败则不进行MQTT连接
    if ( !NETInfo.tcpChk ) return 0;
    // 若mqttChk为1则不进行MQTT连接，但isForce为1时除外
    if ( NETInfo.mqttChk && !isForce ) return 0;
    // 连接MQTT服务器
    NETInfo.mqttChk = use8266_mqttConnect();
    // 订阅MQTT主题
    NETInfo.mqttSubChk = use8266_mqttSubscribe( subTopics, 1 );
    // mqttChk作为函数返回值
    return NETInfo.mqttChk;
}


/**
 * ITF_Task_SetSNTPCFG
 * @brief 设置SNTP
 * @param { uint8_t isForce } 强制设置标记
 */
uint8_t ITF_Task_SetSNTPCFG( uint8_t isForce )
{
    // 当enable为0时不进行SNTP的配置
    if ( !SNTPInfo.enable ) return 0;
    // 当sntpChk为1时不进行SNTP的配置，但isForce为1时除外
    if ( SNTPInfo.sntpChk && !isForce ) return 0;
    // 设置SNTP
    SNTPInfo.sntpChk = use8266_setSNTP( SNTPInfo.enable, SNTPInfo.timezone,
                                        SNTPInfo.servers );
    // sntpChk作为函数返回值
    return SNTPInfo.sntpChk;
}


/**
 * ITF_Task_GetSNTPTime
 * @brief 获取SNTP时间
 * @param { uint8_t isForce } 强制设置标记
 */
uint8_t ITF_Task_GetSNTPTime( uint8_t isForce )
{
    // 若apChk为0时不进行SNTP时间的获取
    if ( !NETInfo.apChk ) return 0;
    // 若sntpChk为0时不进行SNTP时间的获取
    if ( !SNTPInfo.sntpChk ) return 0;
    // 若autoFetch为0时不进行SNTP时间的获取，但isForce为1时除外
    if ( !SNTPInfo.autoFetch && !isForce ) return 0;
    // 获取SNTP时间
    SNTPInfo.asctime = use8266_getSNTPTime();
    Date_SetDateByAsctime( SNTPInfo.asctime );
    return 1;
}


/**
 * ITF_Tasks_DelayAlarm
 * @brief 闹钟延时任务
 */
uint8_t ITF_Task_DelayAlarm( void )
{
    AlarmClock_DelayIAC();
    OLED_ShowString( 68, 56, "Delay 5min", OLED_6X8 );
    OLED_Update();
    Buzzer_Off();
    Delay_s( 1 );
    ITF_GoBack();
    return 1;
}


/**
 * ITF_Tasks_SendDHT11Data
 * @brief 发送DHT11采集的温湿度数据
 * @param { uint8_t isForce } 强制设置标记
 */
uint8_t ITF_Task_SendDHT11Data( uint8_t isForce )
{
    // 当apChk、tcpChk或mqttChk为0时不发送数据
    if ( !NETInfo.apChk || !NETInfo.tcpChk || !NETInfo.mqttChk ) return 0;
    // 当summary为0时表示数据采集失败，此时也不发送数据。但isForce为1时除外
    if ( !DHT11_Data.summary && !isForce ) return 0;
    // 生成数据字符串
    sprintf( sendBuffer, "%d,%d,%d,%d@dht11", DHT11_Data.humidityInt,
             DHT11_Data.humidityDec, DHT11_Data.temperatureInt, DHT11_Data.temperatureDec );
    // 发送数据并返回发送结果
    return ( uint8_t )use8266_mqttPublish( NETInfo.pubTopic, sendBuffer );
}


/**
 * ITF_Task_SendLEDStatus
 * @brief 发布LED状态
 */
uint8_t ITF_Task_SendLEDStatus( void )
{
    // 当apChk、tcpChk或mqttChk为0时不发送数据
    if ( !NETInfo.apChk || !NETInfo.tcpChk || !NETInfo.mqttChk ) return 0;
    // 生成数据字符串
    // sprintf(sendBuffer, "{ \"type\":\"led\", \"value\":%d }", status);
    sprintf( sendBuffer, "%1d@led",
             ( GPIO_ReadOutputDataBit( GPIOA, GPIO_Pin_1 ) == Bit_RESET ) );
    // 发送数据并返回发送结果
    return ( uint8_t )use8266_mqttPublish( NETInfo.pubTopic, sendBuffer );
}


/**
 * Send buzzer status
 * @brief 发布蜂鸣器状态
 */
uint8_t ITF_Task_SendBuzzerStatus( void )
{
    // 当apChk、tcpChk或mqttChk为0时不发送数据
    if ( !NETInfo.apChk || !NETInfo.tcpChk || !NETInfo.mqttChk ) return 0;
    // 生成数据字符串
    // sprintf(sendBuffer, "{ \"type\":\"bz\", \"value\":%d }", status);
    sprintf( sendBuffer, "%1d@bz",
             ( GPIO_ReadOutputDataBit( GPIOA, GPIO_Pin_4 ) == Bit_SET ) );
    // 发送数据并返回发送结果
    return ( uint8_t )use8266_mqttPublish( NETInfo.pubTopic, sendBuffer );
}


/**
 * Send IAC information
 * @brief 发布内部闹钟信息
 */
uint8_t ITF_Task_SendIACInfo( void )
{
    char IACTime[5] = {0};
    uint8_t IACStatus = 0;
    uint8_t IACDelayTime = 0;
    uint8_t IACReachState = 0;
    // 当apChk、tcpChk或mqttChk为0时不发送数据
    if ( !NETInfo.apChk || !NETInfo.tcpChk || !NETInfo.mqttChk ) return 0;
    // 获取时钟信息
    AlarmClock_GetIACTimeString( IACTime );
    IACStatus = AlarmClock_GetIACActiveState();
    IACDelayTime = AlarmClock_GetIACDelayCount();
    IACReachState = AlarmClock_GetIACReachState();
    // 生成数据字符串
    sprintf( sendBuffer, "%d,%s,%d,%d@iac", IACStatus, IACTime, IACDelayTime,
             IACReachState );
    // 发送数据并返回发送结果
    return ( uint8_t )use8266_mqttPublish( NETInfo.pubTopic, sendBuffer );
}


/**
 * ITF_Task_SendAllData
 * @brief 发布温湿度、LED和蜂鸣器状态以及闹钟的相关数据
 */
uint8_t ITF_Task_SendAllData( void )
{
    uint8_t LEDState = 0;
    uint8_t BuzzerState = 0;
    char IACTime[5] = {0};
    uint8_t IACState = 0;
    uint8_t IACDelayTime = 0;
    uint8_t IACReachState = 0;
    // 当apChk、tcpChk或mqttChk为0时不发送数据
    if ( !NETInfo.apChk || !NETInfo.tcpChk || !NETInfo.mqttChk ) return 0;
    // 获取相关信息
    LEDState = ( GPIO_ReadOutputDataBit( GPIOA, GPIO_Pin_1 ) == Bit_RESET );
    BuzzerState = ( GPIO_ReadOutputDataBit( GPIOA, GPIO_Pin_4 ) == Bit_SET );
    IACState = AlarmClock_GetIACActiveState();
    AlarmClock_GetIACTimeString( IACTime );
    IACDelayTime = AlarmClock_GetIACDelayCount();
    IACReachState = AlarmClock_GetIACReachState();
    // 生成数据字符串
    // "{ \"type\":\"led\", \"value\": %1d }",
    // "{ \"type\":\"buzzer\", \"value\": %1d }",
    // "{ \"type\":\"dht11\", \"value\": { \"hi\": %d, \"hd\": %d, \"ti\": %d, \"td\": %d } }",
    // "{ \"type\":\"iac\", \"value\": { \"s\":%d, \"t\":\"%s\", \"d\":%d } }",
    sprintf( sendBuffer, "%d,%d,%d,%d@dht11&" "%1d@led&" "%1d@bz&" "%d,%s,%d,%d@iac",
             DHT11_Data.humidityInt, DHT11_Data.humidityDec, DHT11_Data.temperatureInt,
             DHT11_Data.temperatureDec, LEDState, BuzzerState, IACState, IACTime, IACDelayTime,
             IACReachState );
    // 发送数据并返回发送结果
    return ( uint8_t )use8266_mqttPublish( NETInfo.pubTopic, sendBuffer );
}


/**
 * ITF_Task_SendWeatherReq
 * @brief 发布获取天气信息的请求消息
 */
uint8_t ITF_Task_SendWeatherReq( uint32_t adcode )
{
    // 生成请求字符串
    sprintf( sendBuffer, "%6d@weather",  adcode );
    // 发送数据并返回发送结果
    return ( uint8_t )use8266_mqttPublish( NETInfo.pubTopic, sendBuffer );
}


/**
 * ITF_Task_MQTTReceive
 * @brief MQTT消息接收任务
 */
uint8_t ITF_Task_MQTTReceive( uint8_t ms )
{
    ITF_MQTTRecvTypeDef mqttRecv;
    char *IPD = NULL;
    // 获取IPD头
    IPD = ESP8266_GetIPD( ms );
    // 若找不到IPD头则不进行消息处理
    if ( IPD == NULL ) return 0;
    // 消息解包
    mqttRecv = use8266_mqttReceive( ( unsigned char * )IPD );
    // 若消息解包后发现主题不一致也不进行下一步处理
    if ( !mqttRecv.isPub )
    {
        IFlags.MQTTReceive_TaskRtn = 0;
        return 0;
    }
    // 解析消息并执行对应处理函数
    IFlags.MQTTReceive_TaskRtn = ITF_Task_HandleMQTTPayload( &mqttRecv );
    // 重置ESP8266接收缓冲数组，将IFlags.MQTTReceive_TaskRtn作为函数返回值
    ESP8266_ClearBuffer();
    return IFlags.MQTTReceive_TaskRtn;
}


/**
 * ITF_Task_HandleMQTTPayload
 * @brief MQTT消息处理任务
 */
uint8_t ITF_Task_HandleMQTTPayload( ITF_MQTTRecvTypeDef *mqttRecv )
{
    uint8_t i = 0;
    char *t = NULL, *p = mqttRecv->payload;
    // 查找@符号位置
    t = strchr( mqttRecv->payload, '@' );
    // 若查找失败则消息格式错误，不进行下一步处理
    if ( t == NULL )
    {
        MQTT_FreeBuffer( mqttRecv->payload );
        return 0;
    }
    // 将@符号换成0，以便获取数据
    *t++ = 0;
    // 遍历对比keys，获取对应任务的编号
    for ( i = 0; i < 7; i++ )
    {
        if ( strcmp( t, typeKeys[i] ) == 0 ) break;
    }
    // 通过对应任务编号执行任务
    switch ( i )
    {
        case 0: Buzzer_SetStatus( atoi2( *p ) ); break;
        case 1: LED1_SetStatus( atoi2( *p ) ); break;
        case 2: AlarmClock_SetIACActiveState( atoi2( *p ) ); break;
        case 3: AlarmClock_SetIACTimeByStr( p ); break;
        case 4: AlarmClock_ConfirmIAC(); break;
        case 5: Date_SetDateByAsctime( use8266_getSNTPTime() ); break;
        case 6: ITF_Weather_ParseWeatherStr( p ); break;
        default: i = 0xFF; break; // 若任务编号不满足则置为0xFF
    }
    // DEBUG
    if ( ITF_GetMode() == ITF_MQTTReceive ) // 仅在MQTT消息接受页面输出DEBUG信息
    {
        OLED_ClearArea( 0, 10, 120, 38 );
        OLED_Printf( 0, 10, OLED_6X8, "KEY: %s", t );
        OLED_Printf( 0, 20, OLED_6X8, "VAL: %s", p );
        OLED_Printf( 0, 30, OLED_6X8, "TID: %d", i );
    }
    // 返回任务编号
    MQTT_FreeBuffer( mqttRecv->payload );
    return ( i != 0xFF );
}
