#ifndef __ITF_TASKS_H
#define __ITF_TASKS_H

#include <itf.h>


/* FUNCTION PROTOTYPE --- */

uint8_t ITF_Task_InitDate( void );

uint8_t ITF_Task_InitAlarmClock( void );

uint8_t ITF_Task_InitNETInfo( void );

uint8_t ITF_Task_InitSNTPInfo( void );

uint8_t ITF_Task_CheckAPStatus( uint8_t isForce );

uint8_t ITF_Task_JoinAP( uint8_t isForce );

uint8_t ITF_Task_CheckTCPConnStatus( uint8_t isForce );

uint8_t ITF_Task_MakeTCPConn( uint8_t isForce );

uint8_t ITF_Task_MakeMQTTConn( uint8_t isForce );

uint8_t ITF_Task_SetSNTPCFG( uint8_t isForce );

uint8_t ITF_Task_GetSNTPTime( uint8_t isForce );

uint8_t ITF_Task_DelayAlarm( void );

uint8_t ITF_Task_SendDHT11Data( uint8_t isForce );

uint8_t ITF_Task_SendLEDStatus( void );

uint8_t ITF_Task_SendBuzzerStatus( void );

uint8_t ITF_Task_SendIACInfo( void );

uint8_t ITF_Task_SendAllData( void );

uint8_t ITF_Task_SendWeatherReq( uint32_t adcode );

uint8_t ITF_Task_MQTTReceive( uint8_t ms );

uint8_t ITF_Task_HandleMQTTPayload( ITF_MQTTRecvTypeDef *mqttRecv );

/* --- FUNCTION PROTOTYPE */


#endif
