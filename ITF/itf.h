#ifndef __ITF_H
#define __ITF_H



/* INCLUDE ---------------- */

// System
#include <stm32f10x.h>
#include <delay.h>
#include <usart.h>
// Hardware
#include <oled.h>
#include <key.h>
#include <fps.h>
#include <buzzer.h>
#include <dht11.h>
#include <esp8266.h>
#include <led.h>
// User
#include <date.h>
#include <alarmclock.h>
#include <timerclock.h>
#include <stopwatch.h>
// NET
#include <mqttkit.h>

/* ---------------- INCLUDE */



/* DEFINE ----------------- */

#define MIN(i, j) ( (i<j) ? i : j )
#define MAX(i, j) ( (i>j) ? i : j )
#define atoi2(a) (a - '0')
#define itoa2(i) (i + '0')

/* ----------------- DEFINE */



/* ENUM ------------------- */

typedef enum
{
    // 0-9 Basic interface
    ITF_Init                    = 0,
    ITF_Main                    = 1,
    ITF_Menu                    = 2,
    ITF_MQTTReceive             = 3,
    ITF_Main_DateTime           = 4,
    // 10-19 Settings and sub-settings interface
    ITF_Settings                = 10,
    ITF_TimeSetting             = 11,
    ITF_DateSetting             = 12,
    ITF_AlarmSetting            = 13,
    ITF_SNTPSetting             = 14,
    ITF_DebugSetting            = 15,
    ITF_AlarmTimeSetting        = 16,
    ITF_AlarmRepeatDaySetting   = 17,
    ITF_AlarmReach              = 18,
    ITF_SNTPInfo                = 19,
    // 20-29 Stopwatch interface
    ITF_Stopwatch               = 20,
    // 30-39 Timer interface
    ITF_Timer                   = 30,
    ITF_TimerTimeSetting        = 31,
    ITF_TimerReach              = 32,
    // 40-49
    ITF_ESP8266                 = 40,
    ITF_ESP8266_SelectAP        = 41,
    // 50
    ITF_Weather                 = 50,
    // 60
    ITF_DailyReport             = 60,
    // 70
    ITF_SystemInfo              = 70,
} ITF_ModeTypeDef;

/* ------------------- ENUM */



/* STRUCT ----------------- */

typedef struct
{
    int8_t value;
    int8_t temp;
} ITF_PtrTypeDef;

typedef struct
{
    char *ssid;
    char *passwd;
} ITF_APTypeDef;

typedef struct
{
    uint8_t japRes;
    uint8_t apsPtr;
    uint8_t retry;
    ITF_APTypeDef aps[2];
} ITF_APInfoTypeDef;

typedef struct
{
    const char *srvHost;
    uint16_t srvPort;
    uint16_t srvKeepAlived;
    const char *subTopic;
    const char *pubTopic;
    uint8_t tcpChk;
    uint8_t connChk;
    uint8_t subChk;
    uint8_t pubChk;
} ITF_MQTTInfoTypeDef;

typedef struct
{
    ITF_APTypeDef APs[2];
    uint8_t ptr;
    const char *host;
    uint16_t port;
    uint16_t keepAlived;
    const char *subTopic;
    const char *pubTopic;
    uint8_t japRetry;
    uint8_t apChk;
    uint8_t tcpChk;
    uint8_t mqttChk;
    uint8_t mqttSubChk;
} ITF_NETInfoTypeDef;

typedef struct
{
    uint8_t sntpChk;
    uint8_t autoFetch;
    uint8_t enable;
    uint16_t timezone;
    char *servers[3];
    char *asctime;
} ITF_SNTPInfoTypeDef;

typedef struct
{
    uint8_t debug_main;
    uint8_t Main_JTMS;
    uint8_t MQTTReceive_TaskRtn;
} ITF_FlagsTypeDef;

typedef struct
{
    uint8_t isPub;
    char *payload;
    uint16_t plen;
} ITF_MQTTRecvTypeDef;

/* ----------------- STRUCT */



/* EXTERN ----------------- */

extern uint8_t ITF_IWDGRSTCNT;
extern char sendBuffer[128];
extern const char *typeKeys[];
extern char okString[][7];

extern ITF_FlagsTypeDef IFlags;
extern ITF_APInfoTypeDef APInfo;
extern ITF_MQTTInfoTypeDef MQTTInfo;
extern ITF_SNTPInfoTypeDef SNTPInfo;

extern Date_DateTypeDef DSTemp;
extern Date_DateTypeDef *DS;

extern ITF_NETInfoTypeDef NETInfo;

//extern cJSON *itf_json;

/* ----------------- EXTERN */



/* FUNCTION PROTOTYPE ----- */

void ITF_Mount( void );

uint8_t ITF_InitAndJoinAP( void );

uint8_t ITF_InitAndConnectMQTTSrv( void );

uint8_t ITF_InitDateTime( void );

uint8_t ITF_InitAndSetSNTP( void );

/* ----- FUNCTION PROTOTYPE */



#include <itf_stack.h>
#include <itf_esp8266.h>
#include <itf_weather.h>
#include <itf_utils.h>
#include <itf_tasks.h>
#include <itf_progress.h>
#include <itf_renders.h>



#endif
