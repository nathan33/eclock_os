#ifndef __ITF_RENDERS_H
#define __ITF_RENDERS_H

#include <itf.h>


#define clstlen(list) (sizeof(list)/4)


/* STRUCT ----------------- */



/* ----------------- STRUCT */



/* EXTERN ----------------- */



/* ----------------- EXTERN */



/* FUNCTION PROTOTYPE ----- */

void ITF_RenderInit( void );

void ITF_RenderMain( void );

void ITF_RenderMain2( void );

void ITF_RenderMQTTReceive( void );

void ITF_RenderMenu( void );

void ITF_RenderSettings( void );

void ITF_RenderTimeSetting( void );

void ITF_RenderDateSetting( void );

void ITF_RenderAlarmSetting( void );

void ITF_RenderAlarmTimeSetting( void );

void ITF_RenderAlarmRepeatDaySetting( void );

void ITF_RenderAlarmReach( void );

void ITF_RenderSNTPSetting( void );

void ITF_RenderSNTPInfo( void );

void ITF_RenderDebugSetting( void );

void ITF_RenderStopwatch( void );

void ITF_RenderTimer( void );

void ITF_RenderTimerTimeSetting( void );

void ITF_RenderTimerReach( void );

void ITF_RenderESP8266( void );

void ITF_RenderSelectAP( void );

void ITF_RenderWeather( void );

void ITF_RenderDailyReport( void );

void ITF_RenderSystemInfo( void );

/* ----- FUNCTION PROTOTYPE */



#endif
