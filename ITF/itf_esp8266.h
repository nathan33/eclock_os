#ifndef __ITF_ESP8266_H
#define __ITF_ESP8266_H

#include <itf.h>



/* USE8266 FUNCTION PROTOTYPE ----- */

uint8_t use8266_restore( void );

uint8_t use8266_joinAP( ITF_APTypeDef *ap );

uint8_t use8266_isJoinedAP( ITF_APTypeDef *ap );

uint8_t use8266_makeTCPConnection( const char *host, uint16_t port, uint16_t keepAlived );

uint8_t use8266_isConnected( void );

uint8_t use8266_setSNTP( uint8_t enable, uint16_t timezone, char *servers[3] );

char *use8266_getSNTPTime( void );

uint8_t use8266_mqttConnect( void );

uint8_t use8266_mqttSubscribe( const char *topics[], unsigned char topic_cnt );

uint8_t use8266_mqttPublish( const char *topic, const char *msg );

ITF_MQTTRecvTypeDef use8266_mqttReceive( unsigned char *data );

/* ----- USE8266 FUNCTION PROTOTYPE */



#endif
