#include <itf_progress.h>


uint8_t Init_Taskp = 0;
uint8_t Main_Taskp = 0;
uint8_t SendDHTData_Taskp = 0;
uint8_t SendData_Taskp = 0;
uint8_t MQTTCheck_Taskp = 0;
uint8_t MQTTConnect_Taskp = 0;
uint8_t ESP8266Test_Taskp = 0;


/**
 * @brief 初始化页进程
 */
ITF_Progress_RtnTypeDef ITF_Progress_Init( void )
{
    ITF_Progress_RtnTypeDef rtn = {0, 0, 0};
    
    switch ( Init_Taskp++ )
    {
        // basic
        case 0:
            DS = Date_GetDateStruct();
            TIM_Cmd( TIM2, ENABLE );
            break;
        case 1: rtn.taskRes = ITF_Task_InitDate(); break;
        case 2: rtn.taskRes = ITF_Task_InitAlarmClock(); break;
        case 3: rtn.taskRes++; Stopwatch_InitSTW(); break;
        // NET
        case 4: rtn.taskRes = ITF_Task_InitNETInfo(); break;
        case 5: rtn.taskRes = ITF_Task_CheckAPStatus( 1 ); break;
        case 6: rtn.taskRes = ITF_Task_JoinAP( 0 ); break;
        case 7: rtn.taskRes = ITF_Task_CheckTCPConnStatus( 1 ); break;
        case 8: rtn.taskRes = ITF_Task_MakeTCPConn( 0 ); break;
        case 9: rtn.taskRes = ITF_Task_MakeMQTTConn( 0 ); break;
        // SNTP
        case 10: rtn.taskRes = ITF_Task_InitSNTPInfo(); break;
        case 11: rtn.taskRes = ITF_Task_SetSNTPCFG( 0 ); break;
        case 12: rtn.taskRes = ITF_Task_GetSNTPTime( 0 ); break;
        default:
            rtn.isDone = 1;
            Init_Taskp = 0;
            break;
    }
    
    rtn.taskp = Init_Taskp;
    return rtn;
}


/**
 * @brief 主页进程
 */
ITF_Progress_RtnTypeDef ITF_Progress_Main( void )
{
    ITF_Progress_RtnTypeDef rtn = {0, 0, 0};
    
    switch ( Main_Taskp++ )
    {
        case 0: ITF_Utils_RenderDate( DS, 24, 18, 8 ); break;
        case 1: ITF_Utils_RenderTime( DS, 32, 34, 8 ); break;
        case 2: ITF_Utils_RenderDHT11Data( &DHT11_Data, 80, 56 ); break;
        case 3: ITF_Utils_RenderMainInfo( DS->hour ); break;
        case 4: ITF_Utils_AlertITFCheck(); break;
        case 5:
            IFlags.Main_JTMS = ( NETInfo.apChk && NETInfo.tcpChk && NETInfo.mqttChk &&
                                 SNTPInfo.sntpChk );
            break;
        case 6:
            if ( IFlags.debug_main )
            {
                OLED_Printf( 0, 10, OLED_6X8, "AP%1d,%c%c%c,%1d,%c", ( NETInfo.ptr + 1 ),
                             ( NETInfo.apChk ? '-' : 'J' ), ( NETInfo.tcpChk ? '-' : 'T' ),
                             ( NETInfo.mqttChk ? '-' : 'M' ), ( NETInfo.japRetry ),
                             ( SNTPInfo.sntpChk ? '-' : 'S' ) );
                OLED_Printf( 11 * 6, 10, OLED_6X8, "/%1d%1d%1d%1d%1d", !p1, !p3, !p5, !p10, !p60 );
            }
            break;
        default:
            rtn.isDone = 1;
            Main_Taskp = 0;
            break;
    }
    
    rtn.taskp = Main_Taskp;
    return rtn;
}


/**
 * @brief DHT11数据采集和发送进程
 */
// ITF_Progress_RtnTypeDef ITF_Progress_SendDHTData( void )
// {
//     ITF_Progress_RtnTypeDef rtn = {0, 0, 0};

//     switch ( SendDHTData_Taskp++ )
//     {
//         case 0: DHT11_Scan(); break;
//         case 1: ITF_Tasks_SendDHT11Data( &DHT11_Data ); break;
//         default:
//             rtn.isDone = 1;
//             SendDHTData_Taskp = 0;
//             break;
//     }

//     rtn.taskp = SendDHTData_Taskp;
//     return rtn;
// }


/**
 * @brief 数据发送进程
 */
// ITF_Progress_RtnTypeDef ITF_Progress_SendData( void )
// {
//     ITF_Progress_RtnTypeDef rtn = {0, 0, 0};

//     switch ( SendData_Taskp++ )
//     {
//         case 0: ITF_Tasks_SendDHT11Data( &DHT11_Data ); break;
//         case 1: ITF_Tasks_SendLEDStatus(); break;
//         case 2: ITF_Tasks_SendBuzzerStatus(); break;
//         case 3: ITF_Tasks_SendIACInfo(); break;
//         default:
//             rtn.isDone = 1;
//             SendData_Taskp = 0;
//             break;
//     }

//     rtn.taskp = SendData_Taskp;
//     return rtn;
// }



/**
 * @brief MQTT连接检测
 */
ITF_Progress_RtnTypeDef ITF_Progress_MQTTCheck( void )
{
    ITF_Progress_RtnTypeDef rtn = {0, 0, 0};
    
    switch ( MQTTCheck_Taskp++ )
    {
        case 0: rtn.taskRes = ITF_Task_CheckAPStatus( 0 ); break;
        case 1: rtn.taskRes = ITF_Task_CheckTCPConnStatus( 0 ); break;
        default:
            rtn.isDone = 1;
            MQTTCheck_Taskp = 0;
            break;
    }
    
    rtn.taskp = MQTTCheck_Taskp;
    return rtn;
}


/**
 * @brief 当出现TCP断连时的重连进程
 */
ITF_Progress_RtnTypeDef ITF_Progress_MQTTConnect( void )
{
    ITF_Progress_RtnTypeDef rtn = {0, 0, 0};
    
    switch ( MQTTConnect_Taskp++ )
    {
        case 0: ITF_Task_JoinAP( 0 ); break;
        case 1: ITF_Task_MakeTCPConn( 0 ); break;
        case 2: ITF_Task_MakeMQTTConn( 0 ); break;
        default:
            rtn.isDone = 1;
            MQTTConnect_Taskp = 0;
            break;
    }
    
    rtn.taskp = MQTTConnect_Taskp;
    return rtn;
}


/**
 * @brief ESP8266测试页渲染进程
 */
ITF_Progress_RtnTypeDef ITF_Progress_ESP8266( void )
{
    ITF_Progress_RtnTypeDef rtn = {0, 0, 0};
    
    switch ( ESP8266Test_Taskp++ )
    {
        case 0: OLED_Printf( 72, 10, OLED_6X8, "(%d)", NETInfo.ptr + 1 ); break;
        case 1: OLED_Printf( 80, 10, OLED_6X8, "(%s)", okString[NETInfo.apChk] ); break;
        case 2: OLED_Printf( 80, 20, OLED_6X8, "(%s)", okString[NETInfo.tcpChk] ); break;
        case 3: OLED_Printf( 80, 30, OLED_6X8, "(%s)", okString[NETInfo.mqttChk] ); break;
        case 4: OLED_Printf( 80, 40, OLED_6X8, "(%s)", okString[NETInfo.mqttSubChk] ); break;
        default:
            rtn.isDone = 1;
            ESP8266Test_Taskp = 0;
            break;
    }
    
    rtn.taskp = ESP8266Test_Taskp;
    return rtn;
}

