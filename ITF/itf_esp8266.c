#include <itf_esp8266.h>


#define USERNAME    "c8t6"          // MQTT认证-用户名
#define PASSWORD    "123456"        // MQTT认证-密码
#define DEVICE_ID   "stm32f103c8t6" // MQTT认证-设备ID


/*AT指令缓冲字符数组*/
char ATCmdBuffer[64] = {0};


/**
 * ESP8266 restore
 * @brief 重置ESP8266
 * @return { uint8_t } restore result flag (1/0)
 */
uint8_t use8266_restore( void )
{
    ESP8266_RunStateTypeDef runState;
    /*跑AT指令测试ESP8266状态*/
    ESP8266_RunATCmd( "AT", "OK", 200 );
    /*执行重置指令*/
    runState = ESP8266_RunATCmd( "AT+RESTORE", "OK", 10000 );
    /*运行成功则执行一些初始化的操作*/
    if ( runState == ESP8266_RUN_SUCCESS )
    {
        /*跑AT指令测试ESP8266状态*/
        ESP8266_RunATCmd( "AT", "OK", 100 );
        /*关闭回显*/
        runState = ESP8266_RunATCmd( "ATE0", "OK", 500 );
    }
    /*runState转uint8_t作为函数返回值*/
    return ( uint8_t )runState;
}


/**
 * Make ESP8266 join AP
 * @brief 控制ESP8266连接置顶AP（WLAN）
 * @param { ITF_APTypeDef *ap } AP struct
 * @return { uint8_t } join result flag (1/0)
 */
uint8_t use8266_joinAP( ITF_APTypeDef *ap )
{
    ESP8266_RunStateTypeDef runState;
    /*跑AT指令测试ESP8266状态*/
    ESP8266_RunATCmd( "AT", "OK", 200 );
    /*将ESP8266变更为STA模式*/
    runState = ESP8266_RunATCmd( "AT+CWMODE=1", "OK", 2000 );
    /*设置成功则进行AP的连接操作*/
    if ( runState == ESP8266_RUN_SUCCESS )
    {
        /*检测是否能扫描到指定AP*/
        sprintf( ATCmdBuffer, "AT+CWLAP=\"%s\"", ap->ssid );
        runState = ESP8266_RunATCmd( ATCmdBuffer, "OK", 10000 );
        /*若扫描成功则执行连接操作*/
        if ( ESP8266_FindStringFromBuffer( "+CWLAP:(" ) != NULL )
        {
            /*连接AP*/
            sprintf( ATCmdBuffer, "AT+CWJAP=\"%s\",\"%s\"", ap->ssid, ap->passwd );
            runState = ESP8266_RunATCmd( ATCmdBuffer, "OK", 15000 );
        }
        else
        {
            /*扫描失败则赋值运行失败标志*/
            runState = ESP8266_RUN_FAILED;
        }
    }
    /*runState转uint8_t作为函数返回值*/
    return ( uint8_t )runState;
}


/**
 * Check ESP8266 is join AP already
 * @brief 检测ESP8266是否已经加入到指定AP
 * @param { use8266_apTypeDef *ap } AP struct
 * @return { uint8_t } check result flag (1/0)
 */
uint8_t use8266_isJoinedAP( ITF_APTypeDef *ap )
{
    ESP8266_RunStateTypeDef runState;
    /*跑AT指令测试ESP8266状态*/
    ESP8266_RunATCmd( "AT", "OK", 200 );
    /*检测是否已连接至指定的AP*/
    runState = ESP8266_RunATCmd( "AT+CWJAP?", ap->ssid, 1000 );
    /*runState转uint8_t作为函数返回值*/
    return ( uint8_t )runState;
}


/**
 * Make ESP8266 connect a TCP server
 * @brief 控制ESP8266连接到指定TCP服务器
 * @param { const char *host } TCP server host
 * @param { uint16_t port } TCP server port
 * @param { uint16_t keepAlived } TCP connection keepalived interval
 * @return { uint8_t } connect result flag
 */
uint8_t use8266_makeTCPConnection( const char *host, uint16_t port,
                                   uint16_t keepAlived )
{
    ESP8266_RunStateTypeDef runState;
    /*发送AT指令测试ESP8266状态*/
    ESP8266_RunATCmd( "AT", "OK", 200 );
    /*创建AT指令字符串*/
    sprintf( ATCmdBuffer, "AT+CIPSTART=\"TCP\",\"%s\",%d,%d", host, port, keepAlived );
    /*发送AT指令，创建TCP连接*/
    runState = ESP8266_RunATCmd( ATCmdBuffer, "OK", 5000 );
    /*判断是否出现 no ip 字样，若出现则表示AP连接断开，将runState标志为NOIP*/
    if ( ESP8266_FindStringFromBuffer( "no ip" ) != NULL )
    {
        runState = ESP8266_RUN_TCPNOIP;
    }
    /*runState转uint8_t作为函数返回值*/
    return ( uint8_t )runState;
}


/**
 * Check ESP8266 is connect already
 * @brief 检查ESP8266是否已经建立了TCP连接
 * @return { uint8_t } check result flag
 */
uint8_t use8266_isConnected( void )
{
    ESP8266_RunStateTypeDef runState;
    /*发送AT指令测试ESP8266状态*/
    ESP8266_RunATCmd( "AT", "OK", 200 );
    /*获取TCP/IP状态*/
    runState = ESP8266_RunATCmd( "AT+CIPSTATUS", "OK", 2000 );
    /*判断是否为状态3（已创建TCP连接）*/
    if ( ESP8266_FindStringFromBuffer( "STATUS:3" ) != NULL )
    {
        runState = ESP8266_RUN_SUCCESS;
    }
    else
    {
        runState = ESP8266_RUN_FAILED;
    }
    /*runState转uint8_t作为函数返回值*/
    return ( uint8_t )runState;
}


/**
 * Set ESP8266 SNTP Server
 * @brief 设置ESP8266的SNTP服务器
 * @param { use8266_sntpTypeDef *sntp } SNTP struct
 * @return { uint8_t } Set result flag
 */
uint8_t use8266_setSNTP( uint8_t enable, uint16_t timezone, char *servers[3] )
{
    ESP8266_RunStateTypeDef runState;
    /*发送AT指令测试ESP8266状态*/
    ESP8266_RunATCmd( "AT", "OK", 200 );
    /*生成配置SNTP的AT指令字符串*/
    sprintf( ATCmdBuffer, "AT+CIPSNTPCFG=%d,%d,\"%s\",\"%s\",\"%s\"", enable, timezone, servers[0], servers[1], servers[2] );
    /*发送AT指令*/
    runState = ESP8266_RunATCmd( ATCmdBuffer, "OK", 2000 );
    /*runState转uint8_t作为函数返回值*/
    return ( uint8_t )runState;
}


/**
 * Get SNTP time
 * @brief 控制ESP8266获取一次SNTP时间
 * @param { char *asctime } asctime pointer
 * @return { uint8_t } Get result flag
 */
char *use8266_getSNTPTime( void )
{
    ESP8266_RunStateTypeDef runState;
    char *p = NULL;
    /*发送AT指令测试ESP8266状态*/
    ESP8266_RunATCmd( "AT", "OK", 200 );
    /*获取SNTP时间*/
    runState = ESP8266_RunATCmd( "AT+CIPSNTPTIME?", "OK", 2000 );
    /*若获取成功则进行时间的截取*/
    if ( runState == ESP8266_RUN_SUCCESS )
    {
        /*查找数据行的开头（+CIPSNTPTIME:）*/
        p = ESP8266_FindStringFromBuffer( "+CIPSNTPTIME:" );
        /*若查找成功进行下一步处理*/
        if ( p != NULL )
        {
            /*将字符串指针往后移动13个字符，表示移动到:后，正好是时间数据的开头*/
            p += 13;
            /*判断时间是否正确*/
            if ( strstr( p, "Thu Jan 01 00:00:00 1970" ) == NULL )
            {
                /*指定时间字符串的末尾*/
                *( p + 25 ) = 0;
            }
            else
            {
                /*时间错误，置为NULL*/
                p = NULL;
            }
        }
    }
    /*字符串指针p作为函数返回值*/
    return p;
}


/**
 * Send MQTT connect package
 * @brief 发送 MQTT 请求连接数据包（请求MQTT链接）
 * @return { uint8_t } Request result flag
 */
uint8_t use8266_mqttConnect( void )
{
    MQTT_PACKET_STRUCTURE mqttpkg = {NULL, 0, 0, 0};
    unsigned char *ipd;
    uint8_t chk = 0;
    
    eprint( "[ use8266_mqttConnect ]" );
    
    /*创建MQTT数据包*/
    if ( MQTT_PacketConnect( USERNAME, PASSWORD, DEVICE_ID, 256, 0, MQTT_QOS_LEVEL0, NULL, NULL, 0, &mqttpkg ) == 0 )
    {
        /*发送MQTT数据包*/
        if ( ESP8266_SendData( mqttpkg._data, mqttpkg._len ) )
        {
            /*获取MQTT服务器的返回数据*/
            ipd = ( unsigned char * )ESP8266_GetIPD( 5000 );
            /*判断返回是否为空*/
            if ( ipd != NULL )
            {
                /*解包MQTT响应包，判断是否为连接ACK*/
                if ( MQTT_UnPacketRecv( ipd ) == MQTT_PKT_CONNACK )
                {
                    chk = 0;
                    /*解包ACK*/
                    switch ( MQTT_UnPacketConnectAck( ipd ) )
                    {
                        case 0: eprint( "Connect success" ); chk = 1; break;
                        case 1: eprint( "Protocal error" ); break;
                        case 2: eprint( "Illegal client id" ); break;
                        case 3: eprint( "Server error" ); break;
                        case 4: eprint( "Wrong username or password" ); break;
                        case 5: eprint( "Connection error" ); break;
                        default: eprint( "Unknown error" ); break;
                    }
                }
                else
                {
                    eprint( "Unpackage mqttpkg error" );
                }
            }
            else
            {
                eprint( "IPD not found" );
                chk = 0;
            }
        }
        else
        {
            eprint( "Send data failed" );
        }
        /*释放MQTT数据包*/
        MQTT_DeleteBuffer( &mqttpkg );
    }
    else
    {
        eprint( "mqttpkg generate fail" );
    }
    
    eprint( "" );
    /*chk作为函数返回值*/
    return chk;
}


/**
 * Subscribe MQTT topic(s)
 * @brief 发送 MQTT 订阅消息
 * @param { const char *topics[] } topic(s)
 * @param { unsigned char topic_cnt } count of topic(s)
 * @return { uint8_t } Subscribe result flag
 */
uint8_t use8266_mqttSubscribe( const char *topics[], unsigned char topic_cnt )
{
    MQTT_PACKET_STRUCTURE mqttpkg = {NULL, 0, 0, 0};
    uint8_t chk = 0;
    
    eprint( "[ use8266_mqttSubscribe ]" );
    
    /*循环输出将要订阅的主题信息*/
    for ( chk = 0; chk < topic_cnt; chk++ )
    {
        eprint( "+topic: %s", topics[chk] );
    }
    
    /*创建订阅的MQTT数据包*/
    if ( MQTT_PacketSubscribe( MQTT_SUBSCRIBE_ID, MQTT_QOS_LEVEL0, topics, topic_cnt, &mqttpkg ) == 0 )
    {
        /*发送订阅数据包*/
        chk = ESP8266_SendData( mqttpkg._data, mqttpkg._len );
        /*释放MQTT数据包*/
        MQTT_DeleteBuffer( &mqttpkg );
    }
    else
    {
        eprint( "MQTT subscribe package generate fail" );
    }
    
    eprint( "" );
    /*chk作为函数返回值*/
    return chk;
}


/**
 * Publish MQTT message
 * @brief 发送 MQTT Publish 消息
 * @param { const char *topic } topic
 * @param { const char *msg } message for publish
 * @return { uint8_t } Publish result
 */
uint8_t use8266_mqttPublish( const char *topic, const char *msg )
{
    MQTT_PACKET_STRUCTURE mqttpkg = {NULL, 0, 0, 0};
    uint8_t chk = 0;
    
    eprint( "[ use8266_mqttPublish ]" );
    
    /*创建发布的MQTT数据包*/
    if ( MQTT_PacketPublish( MQTT_PUBLISH_ID, topic, msg, strlen( msg ), MQTT_QOS_LEVEL0, 0, 1, &mqttpkg ) == 0 )
    {
        /*输出DEBUG信息*/
        eprint( "Topic: %s", topic );
        eprint( "Message: %s", msg );
        /*发送MQTT数据包*/
        chk = ESP8266_SendData( mqttpkg._data, mqttpkg._len );
        /*释放MQTT数据包*/
        MQTT_DeleteBuffer( &mqttpkg );
    }
    else
    {
        eprint( "MQTT publish package generate fail" );
    }
    
    eprint( "" );
    /*chk作为函数返回值*/
    return chk;
}


/**
 * Receive MQTT publish message
 * @brief 接收并解包MQTT的Publish消息
 * @param { unsigned char *data } Received IPD data address
 * @return { use8266_mqttPayloadTypeDef } Receive result
 */
ITF_MQTTRecvTypeDef use8266_mqttReceive( unsigned char *recv_data )
{
    ITF_MQTTRecvTypeDef mp = {0, NULL, 0};
    char *topic = NULL;
    uint16_t tlen = 0;
    unsigned char QoS = 0;
    static unsigned short pkgid = 0;
    
    /*解包所接受的数据包*/
    if ( MQTT_UnPacketRecv( recv_data ) ==
            MQTT_PKT_PUBLISH ) // 判断数据包类型是否为PUBLISH
    {
        /*解包PUBLISH数据包*/
        if ( MQTT_UnPacketPublish( recv_data, &topic, &tlen, &mp.payload, &mp.plen, &QoS, &pkgid ) == 0 )
        {
            /*判断QoS以及主题是否正确*/
            if ( ( QoS == 0 ) && ( strcmp( topic, "/nathan/sub" ) == 0 ) )
            {
                /*打印数据包信息*/
                mp.isPub = 1;
                eprint( "Topic(len=%d): %s,\r\nPayload(len=%d): %s\r\n", tlen, topic, mp.plen, mp.payload );
            }
        }
        /*释放MQTT数据包中的主题数据*/
        MQTT_FreeBuffer( topic );
    }
    /*mp（其中包括消息体和消息体长度等信息，将用作后续的处理）作为函数返回值*/
    return mp;
}
