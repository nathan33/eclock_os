#include <itf_renders.h>



uint8_t keyNumber = 0;
char *systemInfo[] =
{
    " GR-Gruaduate-Design ",
    "   Write By Nathan   ",
    "    Version 1.8.9    "
};
char timeSettingStrings[][5] = {"Hour", "Min.", "Sec."};
char dateSettingStrings[][5] = {"Year", "Mon.", "Day "};
char stateString[][8] = {" Pause ", "Running"};
char enString[][9] = {"Disabled", "Enabled "};
char ITF_RenderMain_DebugString[20];



/**
 * 初始化页
 * @brief 初始化页面渲染函数
 * @itfcode ITF_Init
 */
void ITF_RenderInit( void )
{
    OLED_Clear();
    ITF_Utils_RenderTitle( "Initialization" );
    OLED_ShowString( 16, 33, "Initializing ...", OLED_6X8 );
    OLED_Update();
    while ( ITF_Progress_Init().isDone != 1 ) Delay_ms( 64 );
    ITF_Push( ITF_Main );
}


/**
 * 主页
 * @itfcode (ITF_Main)
 */
void ITF_RenderMain( void )
{
    p60 = 58;
    p5 = 1;
    p10 = 4;
    OLED_Clear();
    ITF_Utils_RenderTitle( "Main" );
    while ( ITF_GetMode() == ITF_Main )
    {
        ITF_Utils_RenderFPS( 1 );
        ITF_Progress_Main();
        ITF_Utils_MainTasksPreSec();
        if ( DSTemp.hour != DS->hour )
        {
            DSTemp.hour = DS->hour;
            ITF_Task_GetSNTPTime( 0 );
            Buzzer_DelayBeep( 72 );
            APInfo.retry = 3;
            if ( DSTemp.day != DS->day )
            {
                DSTemp.day = DS->day;
                AlarmClock_ConfirmIAC();
            }
        }
        switch ( Key_GetNumber() )
        {
            case 1: ITF_Push( ITF_Menu ); break;
            case 2: ITF_Push( ITF_Main_DateTime ); break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 主页 => 纯时间和日期显示页
 * @itfcode (ITF_Main_DateTime)
 */
void ITF_RenderMain2( void )
{
    OLED_Clear();
    ITF_Utils_RenderTitle( "Main(BigDateTime)" );
    while ( ITF_GetMode() == ITF_Main_DateTime )
    {
        ITF_Utils_RenderFPS( 0 );
        ITF_Utils_RenderBigDate( DS, 4, 18 );
        ITF_Utils_RenderBigTime( DS, 16, 40 );
        ITF_Utils_MainTasksPreSec();
        switch ( Key_GetNumber() )
        {
            case 1: ITF_GoBack(); break;
            case 2: ITF_Push( ITF_Weather ); break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * MQTT消息接收页
 * @itfcode (ITF_MQTTReceive)
 */
void ITF_RenderMQTTReceive( void )
{
    uint8_t rtnCnt = 5;
    OLED_Clear();
    ITF_Utils_RenderTitle( "MQTT Received" );
    OLED_ShowString( 25, 30, "Receiving ...", OLED_6X8 );
    while ( ITF_GetMode() == ITF_MQTTReceive )
    {
        ITF_Utils_RenderFPS( 0 );
        ITF_Utils_AlertITFCheck();
        if ( ITF_Task_MQTTReceive( 32 ) )
        {
            OLED_ShowImage( 118, 10, 10, 8, revImg );
            if ( rtnCnt + 2 >= 9 ) rtnCnt = 9;
            else rtnCnt += 2;
        }
        else
        {
            OLED_ClearArea( 118, 10, 10, 8 );
            if ( DSTemp.second != DS->second )
            {
                DSTemp.second = DS->second;
                if ( !( --rtnCnt ) )
                {
                    ITF_Task_SendAllData();
                    ITF_GoBack();
                }
            }
        }
        switch ( Key_GetNumber() )
        {
            case 1: ITF_GoBack(); break;
            case 2: ITF_Task_SendAllData(); break;
            default: break;
        }
        OLED_Printf( 0, 56, OLED_6X8, "ReturnAfter:%ds", rtnCnt );
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 菜单页
 * @itfcode (ITF_Menu)
 */
void ITF_RenderMenu( void )
{
    char *list[] =
    {
        "Settings",
        "Stopwatch",
        "Timer",
        "ESP8266",
        "Weather",
        "Daily Report",
        "System Info",
    };
    uint8_t listLength = clstlen( list );
    ITF_PtrTypeDef ptr = {0, 1};
    OLED_Clear();
    ITF_Utils_RenderTitle( "Menu" );
    while ( ITF_GetMode() == ITF_Menu )
    {
        ITF_Utils_RenderFPS( 0 );
        ITF_Utils_AlertITFCheck();
        if ( ptr.temp != ptr.value )
        {
            ITF_Utils_RenderCheckList( list, listLength, ptr.value, NULL, 18, 32 );
            ptr.temp = ptr.value;
        }
        switch ( Key_GetNumber() )
        {
            case 1: ITF_GoBack(); break;
            case 2: ptr.value ++; break;
            case 3: ptr.value --; break;
            case 4: ITF_Push( ( ITF_ModeTypeDef )( ( ptr.value + 1 ) * 10 ) ); break;
            default: break;
        }
        ITF_Utils_PtrCheck( &ptr.value, 0, listLength - 1, 0 );
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 设置页
 * @itfcode (ITF_Settings)
 */
void ITF_RenderSettings( void )
{
    char *list[] =
    {
        "Time (HMS)",
        "Date (YMD)",
        "Alarm Clock",
        "SNTP Config",
        "DEBUG",
    };
    uint8_t listLength = clstlen( list );
    ITF_PtrTypeDef ptr = {0, 1};
    OLED_Clear();
    ITF_Utils_RenderTitle( "Settings" );
    while ( ITF_GetMode() == ITF_Settings )
    {
        ITF_Utils_RenderFPS( 0 );
        ITF_Utils_AlertITFCheck();
        if ( ptr.temp != ptr.value )
        {
            ITF_Utils_RenderCheckList( list, listLength, ptr.value, NULL, 18, 32 );
            ptr.temp = ptr.value;
        }
        switch ( Key_GetNumber() )
        {
            case 1: ITF_GoBack(); break;
            case 2: ptr.value ++; break;
            case 3: ptr.value --; break;
            case 4: ITF_Push( ( ITF_ModeTypeDef )( 11 + ptr.value ) ); break;
            default: break;
        }
        ITF_Utils_PtrCheck( &ptr.value, 0, listLength - 1, 0 );
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 时钟设置页（时分秒）
 * @itfcode (ITF_TimeSetting)
 */
void ITF_RenderTimeSetting( void )
{
    DSTemp = Date_GetDate();
    ITF_PtrTypeDef ptr = {0, 2};
    OLED_Clear();
    ITF_Utils_RenderTitle( "TimeSetting" );
    while ( ITF_GetMode() == ITF_TimeSetting )
    {
        ITF_Utils_RenderFPS( 0 );
        ITF_Utils_RenderTime( &DSTemp, 32, 26, 8 );
        if ( ptr.temp != ptr.value )
        {
            ITF_Utils_RenderSettingPtr( &ptr, 32, 26 );
            ITF_Utils_RenderSettingId( timeSettingStrings, ptr.value, 0, 56 );
            ptr.temp = ptr.value;
        }
        keyNumber = Key_GetNumber();
        switch ( keyNumber )
        {
            case 1:
                Date_SetTime( &DSTemp );
                ITF_GoBack();
                break;
            case 2:
            case 3:
                ITF_Utils_MoveTime( &DSTemp, &ptr.value, ( keyNumber == 2 ? 1 : -1 ) );
                break;
            case 4:
                ptr.value++;
                ITF_Utils_PtrCheck( &( ptr.value ), 0, 2, 1 );
                break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 时钟设置页（年月日）
 * @itfcode (ITF_DateSetting)
 */
void ITF_RenderDateSetting( void )
{
    DSTemp = Date_GetDate();
    ITF_PtrTypeDef ptr = {2, 0};
    OLED_Clear();
    ITF_Utils_RenderTitle( "DateSetting" );
    while ( ITF_GetMode() == ITF_DateSetting )
    {
        ITF_Utils_RenderFPS( 0 );
        ITF_Utils_RenderDate( &DSTemp, 24, 26, 8 );
        if ( ptr.temp != ptr.value )
        {
            ITF_Utils_RenderSettingPtr( &ptr, 24 + 16, 26 );
            ITF_Utils_RenderSettingId( dateSettingStrings, ptr.value, 0, 56 );
            ptr.temp = ptr.value;
        }
        keyNumber = Key_GetNumber();
        switch ( keyNumber )
        {
            case 1:
                Date_SetDate( &DSTemp );
                ITF_GoBack();
                break;
            case 2:
            case 3:
                ITF_Utils_MoveDate( &DSTemp, &ptr.value, ( keyNumber == 2 ? 1 : -1 ) );
                break;
            case 4:
                ptr.value--;
                ITF_Utils_PtrCheck( &ptr.value, 0, 2, 1 );
                break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 闹钟设置页
 * @itfcode (ITF_AlarmSetting)
 */
void ITF_RenderAlarmSetting( void )
{
    AlarmClock_TimeTypeDef *IACTime = AlarmClock_GetIACTimeStruct();
    char repeatChars[10] = {0};
    uint8_t k = 0;
    if ( AlarmClock_GetIACRepeatDayState( 0 ) ) sprintf( repeatChars, "NoRepeat" );
    else for ( k = 1; k < 8; k++ )
            repeatChars[k - 1] = AlarmClock_GetIACRepeatDayState( k ) ? ( '0' + k ) : '-';
    OLED_Clear();
    ITF_Utils_RenderTitle( "AlarmSetting" );
    OLED_Printf( 28, 20, OLED_8X16, "[ %02d:%02d ]", IACTime->hour, IACTime->minute );
    // OLED_Printf( 0, 46, OLED_6X8, "", repeatChars );
    while ( ITF_GetMode() == ITF_AlarmSetting )
    {
        ITF_Utils_RenderFPS( 0 );
        ITF_Utils_AlertITFCheck();
        OLED_Printf( 0, 46, OLED_6X8, "Status:[%s]",
                     enString[AlarmClock_GetIACActiveState()] );
        OLED_Printf( 0, 56, OLED_6X8, "Repeat:[%s]", repeatChars );
        switch ( Key_GetNumber() )
        {
            case 1: ITF_GoBack(); break;
            case 2: AlarmClock_ToggleIACActiveState(); break;
            case 3: ITF_Push( ITF_AlarmRepeatDaySetting ); break;
            case 4: ITF_Push( ITF_AlarmTimeSetting ); break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 闹钟时间设置页
 * @itfcode (ITF_AlarmTimeSetting)
 */
void ITF_RenderAlarmTimeSetting( void )
{
    AlarmClock_TimeTypeDef ACTime = {0};
    ITF_PtrTypeDef ptr = {0, 2};
    DSTemp.minute = 60;
    OLED_Clear();
    if ( AlarmClock_GetIACRepeatDay() ) ACTime = AlarmClock_GetIACTime();
    else
    {
        ACTime.minute = Date_GetDateMinute();
        ACTime.hour = Date_GetDateHour();
    }
    ITF_Utils_RenderTitle( "SetAlarmTime" );
    while ( ITF_GetMode() == ITF_AlarmTimeSetting )
    {
        ITF_Utils_RenderFPS( 0 );
        OLED_Printf( 44, 26, OLED_8X16, "%02d:%02d", ACTime.hour, ACTime.minute );
        OLED_Printf( 68, 56, OLED_6X8, "(now)%02d:%02d", Date_GetDateHour(),
                     Date_GetDateMinute() );
        if ( ptr.temp != ptr.value )
        {
            ITF_Utils_RenderSettingPtr( &ptr, 44, 26 );
            ITF_Utils_RenderSettingId( timeSettingStrings, ptr.value, 0, 56 );
            ptr.temp = ptr.value;
        }
        keyNumber = Key_GetNumber();
        switch ( keyNumber )
        {
            case 1:
                AlarmClock_SetIACTime( &ACTime );
                ITF_GoBack();
                break;
            case 2:
            case 3:
                AlarmClock_MoveTime( &ACTime, ptr.value, ( keyNumber == 2 ? 1 : -1 ) );
                break;
            case 4:
                ptr.value++;
                ITF_Utils_PtrCheck( &ptr.value, 0, 1, 1 );
                break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 闹钟重复日设置页
 * @itfcode (ITF_AlarmRepeatDaySetting)
 */
void ITF_RenderAlarmRepeatDaySetting( void )
{
    char listp[7] = {0};
    char *list[] =
    {
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
    };
    uint8_t listLength = clstlen( list );
    ITF_PtrTypeDef ptr = {0, 1};
    uint8_t i = 0;
    uint8_t repeatDay = AlarmClock_GetIACRepeatDay() | 0x01;
    for ( i = 1; i < 8; i++ )
        listp[i - 1] = ( AlarmClock_GetIACRepeatDayState( i ) ? '*' : ' ' );
    OLED_Clear();
    ITF_Utils_RenderTitle( "SetAlarmRepeatDay" );
    while ( ITF_GetMode() == ITF_AlarmRepeatDaySetting )
    {
        ITF_Utils_RenderFPS( 0 );
        i = repeatDay >> ( ptr.value + 1 ) & 0x01;
        ITF_Utils_RenderCheckList( list, listLength, ptr.value, listp, 18, 32 );
        switch ( Key_GetNumber() )
        {
            case 1:
                if ( repeatDay != 0x01 ) repeatDay &= ~0x01;
                AlarmClock_SetIACRepeatDay( repeatDay );
                ITF_GoBack();
                break;
            case 2: ptr.value++; break;
            case 3: ptr.value--; break;
            case 4:
                i = !i;
                listp[ptr.value] = ( i ? '*' : ' ' );
                if ( i ) repeatDay |= ( 0x01 << ( ptr.value + 1 ) );
                else repeatDay &= ~( 0x01 << ( ptr.value + 1 ) );
                ptr.temp = 0;
                break;
            default: break;
        }
        ITF_Utils_PtrCheck( &ptr.value, 0, listLength - 1, 1 );
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 闹钟响铃页
 * @itfcode (ITF_AlarmReach)
 */
void ITF_RenderAlarmReach( void )
{
    AlarmClock_TimeTypeDef ACT = AlarmClock_GetIACTime();
    uint8_t taskp = 0;
    p60 = 1;
    OLED_Clear();
    ITF_Utils_RenderTitle( "AlarmReached" );
    OLED_Printf( 44, 26, OLED_8X16, "%02d:%02d", ACT.hour, ACT.minute );
    while ( ITF_GetMode() == ITF_AlarmReach )
    {
        ITF_Utils_RenderFPS( 0 );
        OLED_Printf( 0, 56, OLED_6X8, "%s %s", Date_GetWeekDayName(), Date_GetMonthName() );
        if ( DSTemp.second != DS->second )
        {
            DSTemp.second = DS->second;
            if ( !p1 ) ITF_Task_MQTTReceive( 32 );
            switch ( taskp++ )
            {
                case 0: Buzzer_Off(); break;
                case 1: Buzzer_On(); break;
                default:
                    taskp = 0;
                    ITF_Task_SendIACInfo();
                    break;
            }
        }
        keyNumber = Key_GetNumber();
        if ( !p60 ) keyNumber = 2;
        if ( AlarmClock_GetIACConfirmState() == 1 ) keyNumber = 1;
        switch ( keyNumber )
        {
            case 1:
                AlarmClock_ConfirmIAC();
                Delay_ms( 256 );
                ITF_Task_SendAllData();
                ITF_GoBack();
                break;
            case 2:
            case 3: ITF_Task_DelayAlarm(); break;
            case 4:
                AlarmClock_ConfirmIAC();
                ITF_Replace( ITF_AlarmSetting );
                break;
            default: break;
        }
        OLED_Update();
    }
    Buzzer_Off();
    FPS_ClearCounter();
}


/**
 * SNTP设置页
 * @itfcode (ITF_SNTPSetting)
 */
void ITF_RenderSNTPSetting( void )
{
    OLED_Clear();
    ITF_Utils_RenderTitle( "SNTP Setting" );
    OLED_ShowString( 0, 12, "AutoFetch: [   -   ]", OLED_6X8 );
    OLED_ShowString( 0, 22, "FetchTime: EachHour", OLED_6X8 );
    OLED_ShowString( 0, 32, "FetchResult: [  --  ]", OLED_6X8 );
    OLED_ShowChar( 0, 42, '@', OLED_6X8 );
    OLED_ShowImage( 118, 56, 10, 8, infoImg );
    while ( ITF_GetMode() == ITF_SNTPSetting )
    {
        ITF_Utils_RenderFPS( 0 );
        ITF_Utils_RenderDate( DS, 6 + 2, 42, 6 );
        ITF_Utils_RenderTime( DS, 12 * 6 + 2, 42, 6 );
        OLED_ShowString( 12 * 6, 12, enString[SNTPInfo.autoFetch], OLED_6X8 );
        switch ( Key_GetNumber() )
        {
            case 1: ITF_GoBack(); break;
            case 2:
                SNTPInfo.autoFetch = !( SNTPInfo.autoFetch );
                if ( !SNTPInfo.autoFetch ) break;
            case 3:
                SNTPInfo.asctime = use8266_getSNTPTime();
                Date_SetDateByAsctime( SNTPInfo.asctime );
                OLED_ShowString( 14 * 6, 32, okString[( SNTPInfo.asctime != NULL )], OLED_6X8 );
                break;
            case 4: ITF_Push( ITF_SNTPInfo ); break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * SNTP详情页
 * @itfcode (ITF_SNTPInfo)
 */
void ITF_RenderSNTPInfo( void )
{
    OLED_Clear();
    ITF_Utils_RenderTitle( "SNTP Informatoin" );
    OLED_Printf( 0, 10, OLED_6X8, "Enable  : [       ]" );
    OLED_Printf( 0, 18, OLED_6X8, "Timezone: +%3d", SNTPInfo.timezone );
    OLED_ShowString( 0, 26, "Servers :", OLED_6X8 );
    OLED_Printf( 0, 34, OLED_6X8, "- %s", SNTPInfo.servers[0] );
    OLED_Printf( 0, 42, OLED_6X8, "- %s", SNTPInfo.servers[1] );
    OLED_Printf( 0, 50, OLED_6X8, "- %s", SNTPInfo.servers[2] );
    OLED_ShowString( 110, 56, "[-]", OLED_6X8 );
    while ( ITF_GetMode() == ITF_SNTPInfo )
    {
        ITF_Utils_RenderFPS( 0 );
        OLED_ShowString( 66, 10, enString[SNTPInfo.enable], OLED_6X8 );
        switch ( Key_GetNumber() )
        {
            case 1: ITF_GoBack(); break;
            case 2: SNTPInfo.enable = !SNTPInfo.enable; break;
            case 4:
                if ( SNTPInfo.enable )
                {
                    SNTPInfo.sntpChk = use8266_setSNTP( SNTPInfo.enable, SNTPInfo.timezone,
                                                        SNTPInfo.servers );
                    OLED_ShowChar( 116, 56, ( SNTPInfo.sntpChk ? 'S' : 'E' ), OLED_6X8 );
                }
                else OLED_ShowString( 110, 56, "[-]", OLED_6X8 );
                break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * Debug设置页
 * @itfcode (ITF_DebugSetting)
 */
void ITF_RenderDebugSetting( void )
{
    char *list[] = { "MainDebug", "LEDState", "BuzzerState" };
    char listp[3] =
    {
        ( IFlags.debug_main ? '*' : '-' ),
        ( LED1_GetStatus() ? '*' : '-' ),
        ( Buzzer_GetStatus() ? '*' : '-' )
    };
    uint8_t listLength = clstlen( list );
    uint8_t res = 0;
    ITF_PtrTypeDef ptr = {0, 1};
    OLED_Clear();
    ITF_Utils_RenderTitle( "DebugSetting" );
    while ( ITF_GetMode() == ITF_DebugSetting )
    {
        ITF_Utils_RenderFPS( 0 );
        ITF_Utils_AlertITFCheck();
        if ( ptr.temp != ptr.value )
        {
            ITF_Utils_RenderCheckList( list, listLength, ptr.value, listp, 18, 32 );
            ptr.temp = ptr.value;
        }
        switch ( Key_GetNumber() )
        {
            case 1:
                ITF_GoBack();
                ITF_Task_SendAllData();
                break;
            case 2: ptr.value ++; break;
            case 3: ptr.value --; break;
            case 4:
                switch ( ptr.value )
                {
                    case 0: res = IFlags.debug_main = !IFlags.debug_main; break;
                    case 1: res = LED1_ToggleState(); break;
                    case 2: res = Buzzer_ToggleState(); break; \
                }
                listp[ptr.value] = ( res ? '*' : '-' );
                ptr.temp = 4;
                break;
            default: break;
        }
        ITF_Utils_PtrCheck( &ptr.value, 0, listLength - 1, 0 );
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 秒表页
 * @itfcode (ITF_Stopwatch)
 */
void ITF_RenderStopwatch( void )
{
    Stopwatch_TimeTypeDef SWTime;
    Stopwatch_TimeTypeDef *records = Stopwatch_GetSTWRecords();
    uint8_t k, x, y;
    uint8_t recordsUpdateFlag = 1;
    OLED_Clear();
    ITF_Utils_RenderTitle( "Stopwatch" );
    while ( ITF_GetMode() == ITF_Stopwatch )
    {
        ITF_Utils_RenderFPS( 0 );
        SWTime = Stopwatch_GetSTWTime();
        OLED_Printf( 34, 22, OLED_8X16, "%02d:%02d.%02d", SWTime.minute, SWTime.second,
                     SWTime.millisecond );
        OLED_ShowString( 42, 12, stateString[Stopwatch_GetSTWState()], OLED_6X8 );
        if ( recordsUpdateFlag )
        {
            for ( k = 1; k < 5; k++ )
            {
                x = ( k == 1 || k == 3 ) ? 0 : 127 - 10 * 6;
                y = ( k == 1 || k == 2 ) ? 64 - 20 : 64 - 8;
                OLED_Printf( x, y, OLED_6X8, "%1d %02d:%02d.%02d", k, records[k - 1].minute,
                             records[k - 1].second, records[k - 1].millisecond );
            }
            recordsUpdateFlag = 0;
        }
        switch ( Key_GetNumber() )
        {
            case 1: if ( !Stopwatch_GetSTWState() ) ITF_GoBack(); break;
            case 2:
                if ( Stopwatch_GetSTWState() ) Stopwatch_PauseSTW();
                else Stopwatch_StartSTW();
                break;
            case 3:
                Stopwatch_SaveSTWTime();
                recordsUpdateFlag = 1;
                break;
            case 4:
                if ( !Stopwatch_GetSTWState() )
                {
                    Stopwatch_InitSTW();
                    recordsUpdateFlag = 1;
                }
                break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 计时器页
 * @itfcode (ITF_Timer)
 */
void ITF_RenderTimer( void )
{
    TimerClock_TimeTypeDef TCTime;
    OLED_Clear();
    ITF_Utils_RenderTitle( "Timer" );
    while ( ITF_GetMode() == ITF_Timer )
    {
        ITF_Utils_AlertITFCheck();
        ITF_Utils_RenderFPS( 0 );
        OLED_ShowString( 42, 16, stateString[TimerClock_GetMTCActiveState()], OLED_6X8 );
        TCTime = TimerClock_GetMTCTime();
        OLED_Printf( 32, 30, OLED_8X16, "%02d:%02d:%02d", TCTime.hour, TCTime.minute,
                     TCTime.second );
        OLED_Printf( 0, 56, OLED_6X8, "TotalSeconds: %05d", TimerClock_GetMTCTotalSeconds() );
        switch ( Key_GetNumber() )
        {
            case 1: ITF_GoBack(); break;
            case 2: TimerClock_ToggleMTCActiveState(); break;
            case 3: TimerClock_ResetMTC(); break;
            case 4: ITF_Push( ITF_TimerTimeSetting ); break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 计时器时间设置页
 * @itfcode (ITF_TimerTimeSetting)
 */
void ITF_RenderTimerTimeSetting( void )
{
    Date_DateTypeDef date = TimerClock_GetMTCTimeByDateType();
    ITF_PtrTypeDef ptr = {2, 0};
    OLED_Clear();
    ITF_Utils_RenderTitle( "SetTimerTime" );
    while ( ITF_GetMode() == ITF_TimerTimeSetting )
    {
        ITF_Utils_RenderFPS( 0 );
        ITF_Utils_RenderTime( &date, 32, 30, 8 );
        if ( ptr.temp != ptr.value )
        {
           
            ITF_Utils_RenderSettingPtr( &ptr, 32, 30 );
            ITF_Utils_RenderSettingId( timeSettingStrings, ptr.value, 0, 56 );
             ptr.temp = ptr.value;
        }
        keyNumber = Key_GetNumber();
        switch ( keyNumber )
        {
            case 1:
                TimerClock_SetMTCTimeByDateType( &date );
                ITF_GoBack();
                break;
            case 2:
            case 3:
                ITF_Utils_MoveTime( &date, &ptr.value, ( keyNumber == 2 ? 1 : -1 ) );
                break;
            case 4:
                ptr.value --;
                ITF_Utils_PtrCheck( &ptr.value, 0, 2, 1 );
                break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 计时器响铃页
 * @itfcode (ITF_TimerReach)
 */
void ITF_RenderTimerReach( void )
{
    TimerClock_TimeTypeDef TCTime = TimerClock_GetMTCTimeTemp();
    uint8_t taskp = 0;
    OLED_Clear();
    ITF_Utils_RenderTitle( "TimerReached" );
    OLED_Printf( 34, 54, OLED_6X8, "(%02d:%02d:%02d)", TCTime.hour, TCTime.minute,
                 TCTime.second );
    OLED_ShowChar( 20, 24, '-', OLED_8X16 );
    while ( ITF_GetMode() == ITF_TimerReach )
    {
        ITF_Utils_RenderFPS( 0 );
        TCTime = TimerClock_GetMTCTime();
        OLED_Printf( 32, 24, OLED_8X16, "%02d:%02d:%02d", TCTime.hour, TCTime.minute,
                     TCTime.second );
        if ( DSTemp.second != DS->second )
        {
            DSTemp.second = DS->second;
            switch ( taskp++ )
            {
                case 0: Buzzer_Off(); break;
                case 1: Buzzer_On(); break;
                default: taskp = 0; break;
            }
        }
        switch ( Key_GetNumber() )
        {
            case 1:
            case 2:
            case 3: ITF_GoBack(); break;
            case 4: ITF_Replace( ITF_Timer ); break;
            default: break;
        }
        OLED_Update();
    }
    Buzzer_Off();
    TimerClock_ResetMTC();
    FPS_ClearCounter();
}


/**
 * ESP8266测试页
 * @itfcode (ITF_ESP8266)
 */
void ITF_RenderESP8266( void )
{
    OLED_Clear();
    ITF_Utils_RenderTitle( "ESP8266" );
    OLED_ShowString( 0, 10, "Join AP", OLED_6X8 );
    OLED_ShowString( 0, 20, "TCP Conn", OLED_6X8 );
    OLED_ShowString( 0, 30, "MQTT Conn", OLED_6X8 );
    OLED_ShowString( 0, 40, "MQTT Sub", OLED_6X8 );
    while ( ITF_GetMode() == ITF_ESP8266 )
    {
        ITF_Utils_RenderFPS( 0 );
        ITF_Progress_ESP8266();
        switch ( Key_GetNumber() )
        {
            case 1: ITF_GoBack(); break;
            case 2:
                NETInfo.japRetry = 1;
                ITF_Task_JoinAP( 1 );
                ITF_Task_MakeTCPConn( 1 );
                ITF_Task_MakeMQTTConn( 1 );
                break;
            case 3:
                ITF_Task_CheckAPStatus( 1 );
                ITF_Task_CheckTCPConnStatus( 1 );
                break;
            case 4: ITF_Push( ITF_ESP8266_SelectAP ); break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * ESP8266 AP选择页
 * @itfcode (ITF_ESP8266_SelectAP)
 */
void ITF_RenderSelectAP( void )
{
    char *list[] = { NETInfo.APs[0].ssid, NETInfo.APs[1].ssid };
    char listp[2] = {"--"};
    listp[NETInfo.ptr] = '*';
    uint8_t listLength = clstlen( list );
    ITF_PtrTypeDef ptr = {NETInfo.ptr, listLength};
    OLED_Clear();
    ITF_Utils_RenderTitle( "8266/SelectAP" );
    while ( ITF_GetMode() == ITF_ESP8266_SelectAP )
    {
        ITF_Utils_RenderFPS( 0 );
        if ( ptr.temp != ptr.value )
        {
            ITF_Utils_RenderCheckList( list, listLength, ptr.value, listp, 18, 32 );
            ptr.temp = ptr.value;
        }
        switch ( Key_GetNumber() )
        {
            case 1:
                if ( listp[ptr.value] == '*' && NETInfo.ptr != ptr.value )
                {
                    NETInfo.ptr = ptr.value;
                    NETInfo.apChk = 0;
                    NETInfo.tcpChk = 0;
                    NETInfo.mqttChk = 0;
                    NETInfo.mqttSubChk = 0;
                }
                ITF_GoBack();
                break;
            case 2: ptr.value++; break;
            case 3: ptr.value--; break;
            case 4:
                memset( listp, '-', sizeof listp );
                listp[ptr.value] = '*';
                ptr.temp = listLength;
                break;
            default: break;
        }
        ITF_Utils_PtrCheck( &ptr.value, 0, listLength - 1, 1 );
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 天气查看页
 * @itfcode ITF_Weather
 */
void ITF_RenderWeather( void )
{
    uint8_t enver = 0;
    OLED_Clear();
    ITF_Utils_RenderTitle( "Weather(Lives)" );
    ITF_Task_SendWeatherReq( 440402 );
    while ( ITF_GetMode() == ITF_Weather )
    {
        ITF_Utils_RenderFPS( 0 );
        if ( enver ) ITF_Utils_RenderEnWeather();
        else ITF_Utils_RenderZhWeather();
        if ( DSTemp.second != DS->second )
        {
            DSTemp.second = DS->second;
            if ( ITF_Task_MQTTReceive( 32 ) ) OLED_ShowImage( 118, 10, 10, 8, revImg );
            else OLED_ClearArea( 118, 10, 10, 8 );
        }
        switch ( Key_GetNumber() )
        {
            case 1: ITF_GoBack(); break;
            case 2: ITF_Task_SendWeatherReq( 440402 ); break;
            case 3:
                enver = !enver;
                OLED_ClearArea( 0, 10, 128, 54 );
                break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 日度简报（未完成）
 * @itfcode ITF_DailyReport
 */
void ITF_RenderDailyReport( void )
{
    OLED_Clear();
    ITF_Utils_RenderTitle( "DailyReport" );
    while ( ITF_GetMode() == ITF_DailyReport )
    {
        ITF_Utils_RenderFPS( 0 );
        switch ( Key_GetNumber() )
        {
            case 1: ITF_GoBack(); break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 * 系统信息
 * @itfcode ITF_SystemInfo
 */
void ITF_RenderSystemInfo( void )
{
    OLED_Clear();
    ITF_Utils_RenderTitle( "SystemInformation" );
    OLED_ShowString( 1, 14, systemInfo[0], OLED_6X8 );
    OLED_ShowString( 1, 24, systemInfo[1], OLED_6X8 );
    OLED_ShowString( 1, 34, systemInfo[2], OLED_6X8 );
    while ( ITF_GetMode() == ITF_SystemInfo )
    {
        ITF_Utils_RenderFPS( 0 );
        OLED_Printf( 0, 48, OLED_6X8, "SystemRT:%d", systemRT );
        OLED_Printf( 0, 56, OLED_6X8, "IWDGResetCounter:%d", ITF_IWDGRSTCNT );
        switch ( Key_GetNumber() )
        {
            case 1: ITF_GoBack(); break;
            default: break;
        }
        OLED_Update();
    }
    FPS_ClearCounter();
}


/**
 *
 */

