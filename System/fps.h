#ifndef __FPS_H
#define __FPS_H

#include "stm32f10x.h"

extern uint8_t FPS;
extern uint8_t p1;
extern uint8_t p3;
extern uint8_t p5;
extern uint8_t p10;
extern uint8_t p60;
extern uint32_t systemRT;

uint8_t FPS_Check(void);

void FPS_ClearCounter(void);

void FPS_MoveFlags(void);

#endif
