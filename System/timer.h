#ifndef __TIMER_H
#define __TIMER_H

#include <stm32f10x.h>
#include <itf_tasks.h>
#include <stopwatch.h>
#include <oled.h>

void Timer_Init(void);

#endif
