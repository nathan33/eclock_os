#include "stm32f10x.h"
#include "fps.h"

uint8_t FPS = 0;
uint8_t FPS_Counter = 0;
uint8_t p1 = 1;
uint8_t p3 = 1;
uint8_t p5 = 1;
uint8_t p10 = 1;
uint8_t p60 = 1;
uint32_t systemRT = 1;


uint8_t FPS_Check(void)
{
	if ( FPS_Counter < 255)
		FPS_Counter++;
	return FPS;
}


void FPS_ClearCounter(void)
{
	FPS = FPS_Counter;
	FPS_Counter = 0;
}


void FPS_MoveFlags(void)
{
	p1 = (p1 + 1) % 1;
	p3 = (p3 + 1) % 3;
	p5 = (p5 + 1) % 5;
	p10 = (p10 + 1) % 10;
	p60 = (p60 + 1) % 60;
	systemRT++;
}
