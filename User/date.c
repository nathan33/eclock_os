#include <date.h>

Date_TypeDef IDS = {0};
char weekdayNames[][5] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
char monthNames[][4] =
{
    "   ", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};
uint8_t maxDaysPreMonth[13] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

void Date_Init( Date_DateTypeDef *date )
{
    if ( date->month < 1 || date->month > 12 ) date->month = 1;
    if ( date->day < 1 || date->day > 31 ) date->day = 1;
    Date_SetDate( date );
    Date_SetTime( date );
}

// Date attribute getter

Date_TypeDef *Date_GetIDS( void )
{
    return &IDS;
}

Date_DateTypeDef *Date_GetDateStruct( void )
{
    return &IDS.date;
}

Date_DateTypeDef Date_GetDate( void )
{
    return IDS.date;
}

uint8_t Date_GetLeapYearState( void )
{
    return IDS.ly;
}

uint8_t Date_GetWeekDay( void )
{
    return IDS.wday;
}

uint8_t Date_GetLastWeekDay( void )
{
    return IDS.lwday;
}

char *Date_GetWeekDayName( void )
{
    if ( IDS.wday > 8 ) IDS.wday = 0;
    return weekdayNames[IDS.wday];
}

char *Date_GetMonthName( void )
{
    return monthNames[IDS.date.month];
}

int8_t Date_GetDateHour( void )
{
    return IDS.date.hour;
}

int8_t Date_GetDateMinute( void )
{
    return IDS.date.minute;
}

// Date attribute setter

void Date_SetDate( Date_DateTypeDef *date )
{
    IDS.date.year = date->year;
    IDS.date.month  = date->month;
    IDS.date.day    = date->day;
    Date_CalculateDateInfo( &IDS );
    maxDaysPreMonth[1] = IDS.ly ? 29 : 28;
}

void Date_SetTime( Date_DateTypeDef *time )
{
    IDS.date.hour = time->hour;
    IDS.date.minute = time->minute;
    IDS.date.second = time->second;
}

void Date_SetLeapYearState( uint8_t state )
{
    IDS.ly = state;
}

void Date_SetWeekDay( uint8_t wday )
{
    IDS.wday = wday;
}

void Date_SetLastWeekDay( uint8_t lwday )
{
    IDS.lwday = lwday;
}

void Date_SetDateByAsctime( char *asctime )
{
    Date_DateTypeDef date = {0};
    char wday[4] = {0};
    char mon[4] = {0};
    int v;
    
    if ( asctime == NULL ) return;
    
    sscanf(
        asctime, "%3s %3s %2d %2d:%2d:%2d %4d",
        wday, mon, ( int * )&date.day, ( int * )&date.hour,
        ( int * )&date.minute, ( int * )&date.second, ( int * )&v
    );
    
    date.year = v % 100;
    for ( v = 0; v < 8 ; v++ )
    {
        if ( strcmp( weekdayNames[v], wday ) == 0 )
        {
            Date_SetWeekDay( v );
            break;
        }
    }
    for ( v = 0; v < 13 ; v++ )
    {
        if ( strcmp( monthNames[v], mon ) == 0 )
        {
            date.month = v;
            break;
        }
    }
    
    Date_Init( &date );
}

// Date time operator

void Date_MoveYear( Date_DateTypeDef *date, int8_t step, uint8_t isCarry )
{
    date->year += step;
    maxDaysPreMonth[1] = Date_CheckLeapYear( date->year ) ? 29 : 28;
    Date_MoveDay( date, 0, 0 );
    if ( date->year > 99 )
        date->year = 0;
    else if ( date->year < 0 )
        date->year = 99;
}

void Date_MoveMonth( Date_DateTypeDef *date, int8_t step, uint8_t isCarry )
{
    date->month += step;
    Date_MoveDay( date, 0, 0 );
    if ( date->month > 12 )
    {
        if ( isCarry )
            Date_MoveYear( date, 1, isCarry );
        date->month = 1;
    }
    else if ( date->month < 1 )
    {
        if ( isCarry )
            Date_MoveYear( date, -1, isCarry );
        date->month = 12;
    }
}

void Date_MoveDay( Date_DateTypeDef *date, int8_t step, uint8_t isCarry )
{
    int8_t dayMax = maxDaysPreMonth[date->month - 1];
    date->day += step;
    if ( date->day > dayMax )
    {
        if ( isCarry )
            Date_MoveMonth( date, 1, isCarry );
        date->day = 1;
    }
    else if ( date->day < 1 )
    {
        if ( isCarry )
            Date_MoveMonth( date, -1, isCarry );
        date->day = dayMax;
    }
}

void Date_MoveHour( Date_DateTypeDef *date, int8_t step, uint8_t isCarry )
{
    date->hour += step;
    if ( date->hour > 23 )
    {
        if ( isCarry )
            Date_MoveDay( date, 1, isCarry );
        date->hour = 0;
    }
    else if ( date->hour < 0 )
    {
        if ( isCarry )
            Date_MoveDay( date, -1, isCarry );
        date->hour = 23;
    }
}

void Date_MoveMinute( Date_DateTypeDef *date, int8_t step, uint8_t isCarry )
{
    date->minute += step;
    if ( date->minute > 59 )
    {
        if ( isCarry )
            Date_MoveHour( date, 1, isCarry );
        date->minute = 0;
    }
    else if ( date->minute < 0 )
    {
        if ( isCarry )
            Date_MoveHour( date, -1, isCarry );
        date->minute = 59;
    }
}

void Date_MoveSecond( Date_DateTypeDef *date, int8_t step, uint8_t isCarry )
{
    date->second += step;
    if ( date->second > 59 )
    {
        if ( isCarry )
            Date_MoveMinute( date, 1, isCarry );
        date->second = 0;
    }
    else if ( date->second < 0 )
    {
        if ( isCarry )
            Date_MoveMinute( date, -1, isCarry );
        date->second = 59;
    }
}

void Date_IncreaseIDS( void )
{
    Date_MoveSecond( &IDS.date, 1, 1 );
}

// Date general operator

uint8_t Date_CheckLeapYear( uint8_t year )
{
    uint16_t fullYear = 2000 + year;
    if ( fullYear % 4 == 0 )
    {
        if ( fullYear % 100 == 0 )
        {
            if ( fullYear % 400 == 0 )
                return 1;
        }
        else
            return 1;
    }
    return 0;
}

void Date_CalculateDateInfo( Date_TypeDef *fds )
{
    uint8_t m = 0, lwday = 0;
    uint16_t yday = 0;
    // Calculate yday
    for ( m = 1; m < fds->date.month; m++ )
        yday += maxDaysPreMonth[m - 1];
    yday += fds->date.day;
    fds->yday = yday;
    // Calculate ly
    fds->ly = Date_CheckLeapYear( fds->date.year );
    // Calculate lwday
    for ( m = 24; m < fds->date.year; m++ )
    {
        if ( Date_CheckLeapYear( m ) )
            lwday = ( lwday + 366 % 7 ) % 7;
        else
            lwday = ( lwday + 365 % 7 ) % 7;
    }
    fds->lwday = lwday;
    // Calculate wday
    fds->wday = ( yday - 1 ) % 7 + fds->lwday;
}
