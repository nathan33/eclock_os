#ifndef __TIMERCLOCK_H
#define __TIMERCLOCK_H

#include <stm32f10x.h>
#include <date.h>

#define TC_LIST_LENGTH 3

typedef struct
{
    int8_t hour;
    int8_t minute;
    int8_t second;
} TimerClock_TimeTypeDef;

typedef struct
{
    uint8_t flag;
    uint32_t totalSeconds;
    TimerClock_TimeTypeDef time;
} TimerClock_TypeDef;

/* TIMER CLOCK FUNCTIONS ---------- */

// Timer attribute getter
uint8_t TimerClock_GetMTCActiveState( void );
uint8_t TimerClock_GetMTCOverflowState( void );
uint32_t TimerClock_GetMTCTotalSeconds( void );
TimerClock_TimeTypeDef TimerClock_GetMTCTime( void );
TimerClock_TimeTypeDef TimerClock_GetMTCTimeTemp( void );

// Timer attribute setter
void TimerClock_SetMTCActiveState( uint8_t state );
void TimerClock_ToggleMTCActiveState( void );
void TimerClock_SetMTCOverflowState( uint8_t state );
void TimerClock_SetMTCTime( TimerClock_TimeTypeDef *time );
void TimerClock_SetMTCTotalSeconds( uint32_t ts );

// Timer general operator
void TimerClock_ResetMTC( void );
void TimerClock_DecreaseMTC( void );

// Timer and date linkage
void TimerClock_SetMTCTimeByDateType( Date_DateTypeDef *date );
Date_DateTypeDef TimerClock_GetMTCTimeByDateType( void );

/* ---------- TIMER CLOCK FUNCTIONS */

#endif
