#include <alarmclock.h>
#include <string.h>

#define atoi2(c) (c-'0')

AlarmClock_TypeDef IAC;
AlarmClock_ParamsTypeDef IACParams = {5, 5};

/* IAC FUNCTIONS ---------- */

AlarmClock_TypeDef *AlarmClock_GetIAC( void )
{
    return &IAC;
}

// Internal alarm clock attribute getter

uint8_t AlarmClock_GetIACActiveState( void )
{
    return IAC.flag & 0x01;
}

uint8_t AlarmClock_GetIACReachState( void )
{
    return IAC.flag >> 1 & 0x01;
}

uint8_t AlarmClock_GetIACConfirmState( void )
{
    return IAC.flag >> 2 & 0x01;
}

uint8_t AlarmClock_GetIACDelayCount( void )
{
    return IAC.delayTimes;
}

uint8_t AlarmClock_GetIACRepeatDay( void )
{
    return IAC.repeatDay;
}

uint8_t AlarmClock_GetIACRepeatDayState( uint8_t idx )
{
    return ( IAC.repeatDay >> idx ) & 0x01;
}

AlarmClock_TimeTypeDef AlarmClock_GetIACTime( void )
{
    return IAC.time;
}

AlarmClock_TimeTypeDef *AlarmClock_GetIACTimeStruct( void )
{
    return &IAC.time;
}

// Internal alarm clock attribute setter

void AlarmClock_InitIAC( void )
{
    AlarmClock_SetIACReachState( 0 );
    AlarmClock_SetIACConfirmState( 0 );
    AlarmClock_SetIACDelayCount( 0 );
}

void AlarmClock_SetIACActiveState( uint8_t state )
{
    if ( state )
    {
        IAC.flag |= 0x01;
    }
    else
    {
        IAC.flag &= ~0x01;
    }
}

void AlarmClock_ToggleIACActiveState( void )
{
    if ( AlarmClock_GetIACActiveState() )
    {
        IAC.flag &= ~0x01;
    }
    else
    {
        IAC.flag |= 0x01;
    }
}

void AlarmClock_SetIACReachState( uint8_t state )
{
    if ( state )
    {
        IAC.flag |= 0x02;
    }
    else
    {
        IAC.flag &= ~0x02;
    }
}

void AlarmClock_SetIACConfirmState( uint8_t state )
{
    if ( state )
    {
        IAC.flag |= 0x04;
    }
    else
    {
        IAC.flag &= ~0x04;
    }
}

void AlarmClock_SetIACDelayCount( uint8_t count )
{
    IAC.delayTimes = count;
}

void AlarmClock_SetIACRepeatDay( uint8_t repeatDay )
{
    IAC.repeatDay = repeatDay;
}

void AlarmClock_SetIACTime( AlarmClock_TimeTypeDef *time )
{
    IAC.time.hour = time->hour;
    IAC.time.minute = time->minute;
    IAC.timeTemp.hour = time->hour;
    IAC.timeTemp.minute = time->minute;
}

void AlarmClock_SetIACTimeByStr( char *str )
{
    AlarmClock_InitIAC();
    AlarmClock_TimeTypeDef time = {0};
    time.hour += atoi2( *str++ ) * 10;
    time.hour += atoi2( *str );
    str += 2;
    time.minute += atoi2( *str++ ) * 10;
    time.minute += atoi2( *str );
    AlarmClock_SetIACTime( &time );
}

void AlarmClock_GetIACTimeString( char st[5] )
{
    sprintf( st, "%02d:%02d", IAC.time.hour, IAC.time.minute );
}

// Internal alarm clock flag operator

void AlarmClock_ConfirmIAC( void )
{
    IAC.flag = 0x05; // confirm bit = 1, active bit = 1
    IAC.repeatDay &= ~0x01; // no-repeat bit = 0;
    IAC.delayTimes = 0;
    IAC.time.hour = IAC.timeTemp.hour;
    IAC.time.minute = IAC.timeTemp.minute;
    if ( !AlarmClock_GetIACRepeatDay() ) AlarmClock_SetIACActiveState( 0 );
}

void AlarmClock_DelayIAC( void )
{
    uint8_t delayCount = AlarmClock_GetIACDelayCount();
    // If IAC is not on reach state, then return
    if ( AlarmClock_GetIACReachState() == 0 ) return;
    // If delay count was overflow(0x03/00000011/3), then confirm IAC and return
    if ( delayCount > IACParams.MaxDelayTimes )
    {
        AlarmClock_ConfirmIAC();
        return;
    }
    // If delay count not overflow
    delayCount++;
    IAC.time.minute += IACParams.delayMin;
    if ( IAC.time.minute > 59 )
    {
        IAC.time.minute -= 60;
        IAC.time.hour = ( IAC.time.hour + 1 ) % 24;
    }
    AlarmClock_SetIACConfirmState( 0 );
    AlarmClock_SetIACReachState( 0 );
    AlarmClock_SetIACDelayCount( delayCount );
}

void AlarmClock_CheckIACIsReach( int8_t hour, int8_t minute, uint8_t wday )
{
    if ( !AlarmClock_GetIACActiveState() ) return;
    if ( !( IAC.time.hour == hour && IAC.time.minute == minute ) )
    {
        AlarmClock_SetIACConfirmState( 0 );
        return;
    }
    if ( !( AlarmClock_GetIACRepeatDayState( wday + 1 )
            || AlarmClock_GetIACRepeatDayState( 0 ) ) ) return;
    if ( AlarmClock_GetIACConfirmState() || AlarmClock_GetIACReachState() ) return;
    AlarmClock_SetIACReachState( 1 );
}

// Others operator

void AlarmClock_MoveTime( AlarmClock_TimeTypeDef *time, uint8_t ptr, uint8_t step )
{
    if ( ptr == 0 )
    {
        time->hour += step;
        if ( time->hour > 23 ) time->hour = 0;
        else if ( time->hour < 0 ) time->hour = 23;
    }
    else if ( ptr == 1 )
    {
        time->minute += step;
        if ( time->minute > 59 ) time->minute = 0;
        else if ( time->minute < 0 ) time->minute = 59;
    }
}
