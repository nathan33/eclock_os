#include <stm32f10x.h>
#include <timer.h>
#include <usart.h>
#include <itf.h>
#include <itf_utils.h>


int main( void )
{    
    // System initialize
    NVIC_PriorityGroupConfig( NVIC_PriorityGroup_2 );
    Timer_Init();
    Usart1_Init( 115200 );
    Usart2_Init( 115200 );
    
    // Reset flag check
    if ( RCC_GetFlagStatus( RCC_FLAG_IWDGRST ) == SET )
    {
        ITF_IWDGRSTCNT++;
    }
    
    // IWDG Init
    IWDG_WriteAccessCmd( IWDG_WriteAccess_Enable );
    IWDG_SetPrescaler( IWDG_Prescaler_128 );
    IWDG_SetReload( 3124 );
    IWDG_ReloadCounter();
    IWDG_Enable();
    
    // Hardware initialize
    LED_Init();         // led      (PA1)
    OLED_Init();        // oled     (PB8~9)
    Key_Init();         // key      (PB12~15)
    Buzzer_Init();      // buzzer   (PB4)
    DHT11_Init();       // dht11    (PB5)
    
    // Interface mounting
    ITF_Mount();
    
    // Infinity loop
    while ( 1 );
}

