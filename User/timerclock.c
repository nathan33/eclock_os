#include <timerclock.h>

TimerClock_TypeDef MTC = {0};

// Timer attribute getter

uint8_t TimerClock_GetMTCActiveState( void )
{
    return MTC.flag & 0x01;
}

uint8_t TimerClock_GetMTCOverflowState( void )
{
    return MTC.flag >> 1 & 0x01;
}

uint32_t TimerClock_GetMTCTotalSeconds( void )
{
    return MTC.totalSeconds;
}

TimerClock_TimeTypeDef TimerClock_GetMTCTime( void )
{
    TimerClock_TimeTypeDef time;
    uint32_t ts = MTC.totalSeconds;
    time.hour = ts / 3600;
    ts %= 3600;
    time.minute = ts / 60;
    ts %= 60;
    time.second = ts;
    return time;
}

TimerClock_TimeTypeDef TimerClock_GetMTCTimeTemp( void )
{
    return MTC.time;
}

// Timer attribute setter

void TimerClock_SetMTCActiveState( uint8_t state )
{
    if ( state )
    {
        MTC.flag |= 0x01;
    }
    else
    {
        MTC.flag &= ~0x01;
    }
}

void TimerClock_ToggleMTCActiveState( void )
{
    if ( TimerClock_GetMTCActiveState() )
    {
        MTC.flag &= ~0x01;
    }
    else
    {
        MTC.flag |= 0x01;
    }
}

void TimerClock_SetMTCOverflowState( uint8_t state )
{
    if ( state )
    {
        MTC.flag |= 0x02;
    }
    else
    {
        MTC.flag &= ~0x02;
    }
}

void TimerClock_SetMTCTime( TimerClock_TimeTypeDef *time )
{
    MTC.time.hour = time->hour;
    MTC.time.minute = time->minute;
    MTC.time.second = time->second;
    MTC.totalSeconds = time->hour * 3600 + time->minute * 60 + time->second;
}

void TimerClock_SetMTCTotalSeconds( uint32_t ts )
{
    MTC.totalSeconds = ts;
    MTC.time.hour = ts / 3600;
    ts %= 3600;
    MTC.time.minute = ts / 60;
    ts %= 60;
    MTC.time.second = ts;
}

// Timer general operator

void TimerClock_ResetMTC( void )
{
    TimerClock_SetMTCTime( &MTC.time );
    TimerClock_SetMTCOverflowState( 0 );
    TimerClock_SetMTCActiveState( 0 );
}

void TimerClock_DecreaseMTC( void )
{
    if ( TimerClock_GetMTCActiveState() )
    {
        if ( TimerClock_GetMTCOverflowState() )
            MTC.totalSeconds ++;
        else
        {
            if ( !MTC.totalSeconds )
                TimerClock_SetMTCOverflowState( 1 );
            else
                MTC.totalSeconds --;
        }
    }
}

// Timer and date linkage

void TimerClock_SetMTCTimeByDateType( Date_DateTypeDef *date )
{
    MTC.time.hour = date->hour;
    MTC.time.minute = date->minute;
    MTC.time.second = date->second;
    MTC.totalSeconds = date->hour * 3600 + date->minute * 60 + date->second;
}

Date_DateTypeDef TimerClock_GetMTCTimeByDateType( void )
{
    Date_DateTypeDef date;
    uint32_t ts = MTC.totalSeconds;
    date.hour = ts / 3600;
    ts %= 3600;
    date.minute = ts / 60;
    ts %= 60;
    date.second = ts;
    return date;
}
