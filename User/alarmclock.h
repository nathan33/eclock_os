#ifndef __ALARMCLOCK_H
#define __ALARMCLOCK_H

#include <stm32f10x.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct
{
    int8_t minute;
    int8_t hour;
} AlarmClock_TimeTypeDef;

typedef struct
{
    uint8_t flag;
    uint8_t repeatDay;
    uint8_t delayTimes;
    AlarmClock_TimeTypeDef time;
    AlarmClock_TimeTypeDef timeTemp;
} AlarmClock_TypeDef;

typedef struct
{
    uint8_t MaxDelayTimes;
    uint8_t delayMin;
} AlarmClock_ParamsTypeDef;

/* IAC FUNCTIONS ---------- */

AlarmClock_TypeDef *AlarmClock_GetIAC( void );

// Internal alarm clock attribute getter
uint8_t AlarmClock_GetIACActiveState( void );
uint8_t AlarmClock_GetIACReachState( void );
uint8_t AlarmClock_GetIACConfirmState( void );
uint8_t AlarmClock_GetIACDelayCount( void );
uint8_t AlarmClock_GetIACRepeatDay( void );
uint8_t AlarmClock_GetIACRepeatDayState( uint8_t idx );
AlarmClock_TimeTypeDef AlarmClock_GetIACTime( void );
AlarmClock_TimeTypeDef *AlarmClock_GetIACTimeStruct( void );

// Internal alarm clock attribute setter
void AlarmClock_SetIACActiveState( uint8_t state );
void AlarmClock_ToggleIACActiveState( void );
void AlarmClock_SetIACReachState( uint8_t state );
void AlarmClock_SetIACConfirmState( uint8_t state );
void AlarmClock_SetIACDelayCount( uint8_t count );
void AlarmClock_SetIACRepeatDay( uint8_t repeatDay );
void AlarmClock_SetIACTime( AlarmClock_TimeTypeDef *time );
void AlarmClock_SetIACTimeByStr( char *str );
void AlarmClock_GetIACTimeString( char st[5] );

// Internal alarm clock flag operator
void AlarmClock_ConfirmIAC( void );
void AlarmClock_DelayIAC( void );
void AlarmClock_CheckIACIsReach( int8_t hour, int8_t minute, uint8_t wday );

/* ---------- IAC FUNCTIONS */

void AlarmClock_MoveTime( AlarmClock_TimeTypeDef *time, uint8_t ptr, uint8_t step );


#endif
