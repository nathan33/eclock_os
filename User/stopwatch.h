#ifndef __STOPWATCH_H
#define __STOPWATCH_H

#include "stm32f10x.h"

typedef struct
{
    uint8_t minute;
    uint8_t second;
    uint8_t millisecond;
} Stopwatch_TimeTypeDef;

typedef struct
{
    uint8_t state;
    uint8_t minute;
    uint8_t second;
    Stopwatch_TimeTypeDef records[4];
} Stopwatch_TypeDef;

void Stopwatch_InitSTW( void );

// Stopwatch attribute getter
uint8_t Stopwatch_GetSTWState( void );
Stopwatch_TimeTypeDef Stopwatch_GetSTWTime( void );
Stopwatch_TimeTypeDef *Stopwatch_GetSTWRecords( void );

// Stopwatch general operator
void Stopwatch_StartSTW( void );
void Stopwatch_PauseSTW( void );
void Stopwatch_SaveSTWTime( void );
void Stopwatch_IncreaseSTW( void );

#endif
