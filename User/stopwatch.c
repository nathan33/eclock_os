#include <stopwatch.h>
#include <string.h>

Stopwatch_TypeDef STW;

void Stopwatch_InitSTW( void )
{
    TIM_Cmd( TIM3, DISABLE );
    TIM_SetCounter( TIM3, 0 );
    STW.state = 0;
    STW.minute = 0;
    STW.second = 0;
    memset( &STW.records[0], 0, 3 );
    memset( &STW.records[1], 0, 3 );
    memset( &STW.records[2], 0, 3 );
    memset( &STW.records[3], 0, 3 );
}

// Stopwatch attribute getter

uint8_t Stopwatch_GetSTWState( void )
{
    return STW.state;
}

Stopwatch_TimeTypeDef Stopwatch_GetSTWTime( void )
{
    Stopwatch_TimeTypeDef time;
    time.minute = STW.minute;
    time.second = STW.second;
    time.millisecond = TIM_GetCounter( TIM3 ) / 100;
    return time;
}

Stopwatch_TimeTypeDef *Stopwatch_GetSTWRecords( void )
{
    return STW.records;
}

// Stopwatch attribute setter

void Stopwatch_StartSTW( void )
{
    TIM_Cmd( TIM3, ENABLE );
    STW.state = 1;
}

void Stopwatch_PauseSTW( void )
{
    TIM_Cmd( TIM3, DISABLE );
    STW.state = 0;
}

void Stopwatch_SaveSTWTime( void )
{
    uint8_t i;
    for ( i = 3; i > 0; i-- )
    {
        STW.records[i].minute =  STW.records[i - 1].minute;
        STW.records[i].second = STW.records[i - 1].second;
        STW.records[i].millisecond = STW.records[i - 1].millisecond;
    }
    STW.records[0].minute = STW.minute;
    STW.records[0].second = STW.second;
    STW.records[0].millisecond = TIM_GetCounter( TIM3 ) / 100;
}

void Stopwatch_IncreaseSTW( void )
{
    if ( !STW.state ) return;
    STW.second++;
    if ( STW.second > 59 )
    {
        STW.second = 0;
        STW.minute = ( STW.minute + 1 ) % 100;
    }
}
