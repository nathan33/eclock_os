#ifndef __DATE_H
#define __DATE_H

#include <stm32f10x.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct
{
    int8_t year;
    int8_t month;
    int8_t day;
    int8_t hour;
    int8_t minute;
    int8_t second;
} Date_DateTypeDef;

typedef struct
{
    uint8_t ly;
    uint8_t wday;
    uint8_t lwday;
    uint16_t yday;
    Date_DateTypeDef date;
} Date_TypeDef;

/* DATE FUNCTION ---------- */

void Date_Init( Date_DateTypeDef *date );

// Date attribute getter
Date_DateTypeDef *Date_GetDateStruct( void );
Date_DateTypeDef Date_GetDate( void );
uint8_t Date_GetLeapYearState( void );
uint8_t Date_GetWeekDay( void );
uint8_t Date_GetLastWeekDay( void );
char *Date_GetWeekDayName( void );
char *Date_GetMonthName( void );
int8_t Date_GetDateHour( void );
int8_t Date_GetDateMinute( void );

// Date attribute setter
Date_TypeDef *Date_GetIDS( void );
void Date_SetDate( Date_DateTypeDef *date );
void Date_SetTime( Date_DateTypeDef *time );
void Date_SetLeapYearState( uint8_t state );
void Date_SetWeekDay( uint8_t wday );
void Date_SetLastWeekDay( uint8_t lwday );
void Date_SetDateByAsctime( char *asctime );

// Date time operator
void Date_MoveYear( Date_DateTypeDef *date, int8_t step, uint8_t isCarry );
void Date_MoveMonth( Date_DateTypeDef *date, int8_t step, uint8_t isCarry );
void Date_MoveDay( Date_DateTypeDef *date, int8_t step, uint8_t isCarry );
void Date_MoveHour( Date_DateTypeDef *date, int8_t step, uint8_t isCarry );
void Date_MoveMinute( Date_DateTypeDef *date, int8_t step, uint8_t isCarry );
void Date_MoveSecond( Date_DateTypeDef *date, int8_t step, uint8_t isCarry );
void Date_IncreaseIDS( void );

// Date general operator
uint8_t Date_CheckLeapYear( uint8_t year );
void Date_CalculateDateInfo( Date_TypeDef *fds );

/* ---------- DATE FUNCTION */

#endif
